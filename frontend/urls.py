from django.conf.urls import patterns

urlpatterns = patterns('',
   (r'^$', 'frontend.views.home'),
   (r'^home/$', 'frontend.views.home'),
   (r'^nicotine_reduction/$', 'frontend.views.nicotine_reduction'),
   (r'^our_flavours/$', 'frontend.views.our_flavours'),
   (r'^ourpipes/$', 'frontend.views.our_pipes'),
   (r'^contact_us/$', 'frontend.views.contact_us'),
)