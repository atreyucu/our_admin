from django.db import models

class Message(models.Model):
    user_email = models.EmailField()
    user_name = models.CharField(max_length=255)
    body = models.TextField()
    date_sent = models.DateTimeField(auto_now=True)
    is_replied = models.BooleanField(default=False)
    parent_id = models.IntegerField(default=0)
    received = models.BooleanField(default=True)
