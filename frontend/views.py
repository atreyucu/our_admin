from django.shortcuts import render_to_response
from django.template.context import RequestContext
from frontend.forms import CaptchaForm
from frontend.models import Message
from django.views.decorators.gzip import gzip_page

@gzip_page
def home(request):
    return render_to_response('frontend/index.html',context_instance = RequestContext(request))

@gzip_page
def nicotine_reduction(request):
    return render_to_response('frontend/nicotine_reduction.html',context_instance = RequestContext(request))

@gzip_page
def our_flavours(request):
    return render_to_response('frontend/our_flavours.html',context_instance = RequestContext(request))

@gzip_page
def our_pipes(request):
    return render_to_response('frontend/ourpipes.html',context_instance = RequestContext(request))

@gzip_page
def contact_us(request):
    if request.POST:
        form = CaptchaForm(request.POST)
        name = request.POST['name']
        email = request.POST['email']
        message = request.POST['message']
        if form.is_valid():
            public_message = Message(user_name=name, user_email=email, body=message)
            public_message.save()
            return render_to_response('frontend/contact_us.html', {'form': form, 'captcha': 1},context_instance = RequestContext(request))
        else:
            return render_to_response('frontend/contact_us.html', {'form': form, 'captcha': 0, 'name':name, 'email':email, 'message':message},context_instance = RequestContext(request))
    else:
        form = CaptchaForm()
        return render_to_response('frontend/contact_us.html', {'form': form, 'captcha': -1},context_instance = RequestContext(request))
