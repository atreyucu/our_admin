from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
import datetime

class SLDActivatecodes(models.Model):
    id = models.AutoField(primary_key=True)
    act_code = models.CharField(max_length=80)
    activated = models.IntegerField()
    class Meta:
        db_table = 'admins_activatecodes'

class SLDMessage(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User)
    subject = models.CharField(max_length=255)
    body = models.TextField()
    sent = models.DateTimeField()
    is_replied = models.BooleanField(default=False)
    parent_id = models.IntegerField(default=0)
    received = models.BooleanField(default=True)
    reply_first_name = models.CharField(max_length=50,default='')
    reply_last_name = models.CharField(max_length=50,default='')
    class Meta:
        db_table = 'admins_message'

class SLDPrintBatch(models.Model):
    batch_id = models.CharField(max_length=100,default=str(datetime.date.today().toordinal()))
    created_on = models.DateField(default=datetime.date.today().toordinal())
    pending = models.BooleanField(default=True)

    class Meta:
        db_table = 'admins_printbatch'

    def __unicode__(self):
        return self.batch_id

class SLDPrintedorders(models.Model):
    id = models.AutoField(primary_key=True)
    order_id = models.IntegerField()
    printed = models.IntegerField()
    printed_on = models.DateTimeField()
    printed_batch = models.ForeignKey(SLDPrintBatch,null=True)
    standing_order = models.ForeignKey('SLDStandingorderinformation', null= True)
    class Meta:
        db_table = 'admins_printedorders'

class SLDProductcategory(models.Model):
    id = models.AutoField(primary_key=True)
    category_name = models.CharField(max_length=100)
    class Meta:
        db_table = 'admins_productcategory'

class SLDProductsize(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=True)
    short_name = models.CharField(max_length=10)
    class Meta:
        db_table = 'admins_productsize'

class SLDPublicmessage(models.Model):
    id = models.AutoField(primary_key=True)
    user_email = models.CharField(max_length=75)
    user_name = models.CharField(max_length=255)
    body = models.TextField()
    date_sent = models.DateTimeField()
    is_replied = models.BooleanField(default=False)
    parent_id = models.IntegerField(default=0)
    class Meta:
        db_table = 'admins_publicmessage'

class SLDSldproductextension(models.Model):
    id = models.AutoField(primary_key=True)
    category = models.ForeignKey(SLDProductcategory)
    size = models.ForeignKey(SLDProductsize)
    cur_product_id = models.IntegerField(unique=True)
    parent_product_id = models.IntegerField()
    class Meta:
        db_table = 'admins_sldproductextension'

class SLDStandingorderinformation(models.Model):
    owner_user_id = models.IntegerField()
    user_id = models.CharField(max_length=25)
    rebilling_id = models.CharField(max_length=255)
    order_date = models.DateTimeField()
    last_success_rebilling_date = models.DateTimeField(null=True, blank=True)
    rebilling_period = models.IntegerField()
    next_rebilling = models.DateTimeField(null=True, blank=True)
    delivery_period = models.IntegerField()
    next_delivery_printed = models.DateTimeField(null=True, blank=True)
    last_delivery_printed = models.DateTimeField(null=True, blank=True)
    last_delivery_batch_id = models.CharField(max_length=100, blank=True)
    unsubscribe_recived = models.DateTimeField(null=True, blank=True)
    unsubscribe_approved = models.DateTimeField(null=True, blank=True)
    number_of_billing_successful = models.IntegerField()
    free_vaporizer = models.IntegerField()
    number_of_free_pipe = models.IntegerField()
    vpsprotocol = models.CharField(max_length=20, db_column='VPSProtocol') # Field name made lowercase.
    txtype = models.CharField(max_length=100, db_column='TxType') # Field name made lowercase.
    vendortxcode = models.CharField(max_length=100, db_column='VendorTxCode') # Field name made lowercase.
    vendor = models.CharField(max_length=100, db_column='Vendor') # Field name made lowercase.
    amount = models.CharField(max_length=100, db_column='Amount') # Field name made lowercase.
    currency = models.CharField(max_length=100, db_column='Currency') # Field name made lowercase.
    description = models.CharField(max_length=255, db_column='Description') # Field name made lowercase.
    billingsurname = models.CharField(max_length=100, db_column='BillingSurname') # Field name made lowercase.
    billingfirstnames = models.CharField(max_length=100, db_column='BillingFirstnames') # Field name made lowercase.
    billingaddress1 = models.CharField(max_length=100, db_column='BillingAddress1') # Field name made lowercase.
    billingcity = models.CharField(max_length=100, db_column='BillingCity') # Field name made lowercase.
    billingpostcode = models.CharField(max_length=100, db_column='BillingPostCode') # Field name made lowercase.
    billingcountry = models.CharField(max_length=100, db_column='BillingCountry') # Field name made lowercase.
    deliverysurname = models.CharField(max_length=100, db_column='DeliverySurname') # Field name made lowercase.
    deliveryfirstnames = models.CharField(max_length=100, db_column='DeliveryFirstnames') # Field name made lowercase.
    deliveryaddress1 = models.CharField(max_length=100, db_column='DeliveryAddress1') # Field name made lowercase.
    deliverycity = models.CharField(max_length=100, db_column='DeliveryCity') # Field name made lowercase.
    deliverypostcode = models.CharField(max_length=100, db_column='DeliveryPostCode') # Field name made lowercase.
    deliverycountry = models.CharField(max_length=100, db_column='DeliveryCountry') # Field name made lowercase.
    notificationurl = models.CharField(max_length=100, db_column='NotificationURL') # Field name made lowercase.
    createtoken = models.IntegerField(db_column='CreateToken') # Field name made lowercase.
    error_detail = models.TextField(blank=True)
    cancelled = models.BooleanField()
    cancelled_approved = models.BooleanField(default=False)
    account_actived = models.BooleanField(default=False)
    created_date = models.DateField(auto_now=False,default=datetime.date.today())
    class Meta:
        db_table = 'users_standingorderinformation'

class SLDGeneraluser(models.Model):
    user = models.ForeignKey(User, unique=True)
    cur_user_id = models.IntegerField()
    email = models.CharField(max_length=70)
    password = models.CharField(max_length=150)
    your_title = models.IntegerField()
    your_firstname = models.CharField(max_length=150)
    your_secondname = models.CharField(max_length=150)
    your_hbn = models.CharField(max_length=150)
    your_streetaddress = models.CharField(max_length=150)
    your_secondlineaddress = models.CharField(max_length=150)
    your_towncity = models.IntegerField()
    your_country = models.CharField(max_length=10)
    your_postcode = models.CharField(max_length=150)
    delivery_title = models.IntegerField()
    delivery_firstname = models.CharField(max_length=150)
    delivery_secondname = models.CharField(max_length=150)
    delivery_hbn = models.CharField(max_length=150)
    delivery_streetaddress = models.CharField(max_length=150)
    delivery_secondlineaddress = models.CharField(max_length=150)
    delivery_towncity = models.IntegerField()
    delivery_country = models.CharField(max_length=10)
    delivery_postcode = models.CharField(max_length=150)
    billing_title = models.IntegerField()
    billing_firstname = models.CharField(max_length=150)
    billing_secondname = models.CharField(max_length=150)
    billing_hbn = models.CharField(max_length=150)
    billing_streetaddress = models.CharField(max_length=150)
    billing_secondlineaddress = models.CharField(max_length=150)
    billing_towncity = models.IntegerField()
    billing_country = models.CharField(max_length=10)
    billing_postcode = models.CharField(max_length=150)
    cigarette_smoked_per_day = models.IntegerField()
    standing_order_information = models.ForeignKey(SLDStandingorderinformation)
    class Meta:
        db_table = 'general_auth_generaluser'



class SLDFlavours(models.Model):
    flavour_1 = models.IntegerField()
    flavour_2 = models.IntegerField()
    flavour_3 = models.IntegerField()
    flavour_4 = models.IntegerField()
    flavour_5 = models.IntegerField()
    general_user = models.ForeignKey(SLDGeneraluser)
    class Meta:
        db_table = 'general_auth_flavours'



class SLDInvitesInviteregister(models.Model):
    user = models.ForeignKey(User)
    channel = models.CharField(max_length=2)
    send_date = models.DateTimeField()
    reply_date = models.DateTimeField()
    is_follow = models.IntegerField()
    is_sign_up = models.IntegerField()
    class Meta:
        db_table = 'social_invites_inviteregister'