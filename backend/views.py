import datetime
from django.shortcuts import render_to_response
from django.template import Context
from django.template.loader import get_template
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.views import logout
from common.sld import SLDStandingorderinformation
from common.ssd import SSDStandingorderinformation
from common.sld import SLDPrintedorders
from common.ssd import SSDPrintedorders
from common.sld import SLDPrintBatch
from common.ssd import SSDPrintBatch
from common.sld import SLDMessage
from common.sld import SLDPublicmessage
from common.ssd import SSDMessage
from common.ssd import SSDPublicmessage
from frontend.models import Message
import ho.pisa as pisa
import ouradmin.settings as settings
from pyPdf import PdfFileReader, PdfFileWriter
import StringIO
import os
from cor.models import Orderr
import zipfile
import json
import uuid
from django.contrib.auth.models import User
from django.core.mail import send_mail
import smtplib
from django.core.mail.message import EmailMessage
from django.views.decorators.gzip import gzip_page

@gzip_page
@login_required
def home(request):
    new_orders_today_sld = str(SLDStandingorderinformation.objects.filter(order_date=datetime.date.today()).using('sld').count())
    new_orders_today_ssd = str(SSDStandingorderinformation.objects.filter(order_date=datetime.date.today()).using('ssd').count())

    subscription_today_sld = str(SLDStandingorderinformation.objects.filter(next_rebilling=datetime.date.today()).using('sld').count())
    subscription_today_ssd = str(SSDStandingorderinformation.objects.filter(next_rebilling=datetime.date.today()).using('ssd').count())

    pending_orders_sld = str(SLDPrintedorders.objects.filter(printed=False).using('sld').count())
    pending_orders_ssd = str(SSDPrintedorders.objects.filter(printed=False).using('ssd').count())


    subscription_tomorrow_sld = str(SLDStandingorderinformation.objects.filter(next_rebilling=datetime.date.today() + datetime.timedelta(days=1)).using('sld').count())
    subscription_tomorrow_ssd = str(SSDStandingorderinformation.objects.filter(next_rebilling=datetime.date.today() + datetime.timedelta(days=1)).using('ssd').count())

    today_new_subscribers_sld = str(SLDStandingorderinformation.objects.filter(created_date=datetime.date.today()).using('sld').count())
    today_new_subscribers_ssd = str(SSDStandingorderinformation.objects.filter(created_date=datetime.date.today()).using('ssd').count())

    total_subscribers_sld = str(SLDStandingorderinformation.objects.filter(cancelled=False).using('sld').count())
    total_subscribers_ssd = str(SSDStandingorderinformation.objects.filter(cancelled=False).using('ssd').count())

    return render_to_response('backend/home.html',{
        'new_orders_today_sld': new_orders_today_sld,
        'new_orders_today_ssd': new_orders_today_ssd,
        'subscription_today_sld': subscription_today_sld,
        'subscription_today_ssd': subscription_today_ssd,
        'pending_orders_sld': pending_orders_sld,
        'pending_orders_ssd': pending_orders_ssd,
        'subscription_tomorrow_sld': subscription_tomorrow_sld,
        'subscription_tomorrow_ssd': subscription_tomorrow_ssd,
        'today_new_subscribers_sld' : today_new_subscribers_sld,
        'today_new_subscribers_ssd': today_new_subscribers_ssd,
        'total_subscribers_sld': total_subscribers_sld,
        'total_subscribers_ssd': total_subscribers_ssd},
         context_instance = RequestContext(request))

@gzip_page
@login_required
def orders(request):
    host_name = request.META['HTTP_HOST']
    if request.is_secure():
        urlpart = 'https://%s'%(host_name)
    else:
        urlpart = 'http://%s'%(host_name)

    batchs = {}

    sld_batch = SLDPrintBatch.objects.all().using('sld')
    status = ''
    print_all_pending = False
    for batch in sld_batch:
        if batch.pending == True:
            status = 'PENDING'
            print_all_pending = True
        else:
            status = 'Printed'

        temp_orders = []
        orders = batch.sldprintedorders_set.all()
        for order in orders:
            temp_orders.append((order.id,str(order.order_id),order.standing_order.sldgeneraluser_set.all()[0].your_firstname,order.standing_order.sldgeneraluser_set.all()[0].your_secondname,'sld'))


        batchs[batch.batch_id] = (
            batch.sldprintedorders_set.count(),
            str(batch.created_on),
            status,
            temp_orders,
            'sld',
        )

    ssd_batch = SSDPrintBatch.objects.all().using('ssd')

    for batch in ssd_batch:
        temp_orders = []
        for order in batch.ssdprintedorders_set.all():
            temp_orders.append((order.id,str(order.order_id),order.standing_order.ssdgeneraluser_set.all()[0].your_firstname,order.standing_order.ssdgeneraluser_set.all()[0].your_secondname,'ssd'))

        if batchs.has_key(batch.batch_id):
            if batchs[batch.batch_id][2] == 'Printed':
                if batch.pending == True:
                    status = 'PENDING'
                    print_all_pending = True
                else:
                    status = 'Printed'
            else:
                status = 'PENDING'
                print_all_pending = True
            temp_arr = batchs[batch.batch_id][3]
            for order in temp_orders:
                 temp_arr.append(order)
            temp_t = (str(int(batchs[batch.batch_id][0]) + batch.ssdprintedorders_set.count()), batchs[batch.batch_id][1],status,temp_arr,'both')
            batchs[batch.batch_id] = temp_t
        else:
            if batch.pending == True:
                    status = 'PENDING'
                    print_all_pending = True
            else:
                status = 'Printed'
            batchs[batch.batch_id] = (
                batch.ssdprintedorders_set.count(),
                str(batch.created_on),
                status,
                temp_orders,
                'ssd'
            )

    return render_to_response('backend/orders.html',{'batchs':batchs, 'print_all_pending':print_all_pending,'urlpart':urlpart},context_instance = RequestContext(request))

@gzip_page
@login_required
def reports(request):
    return render_to_response('backend/reports.html',context_instance = RequestContext(request))

@gzip_page
@login_required
def stock(request):
    return render_to_response('backend/stock.html',context_instance = RequestContext(request))

@gzip_page
@login_required
def subscribers(request):
    sld_cancelled_subscriptions = SLDStandingorderinformation.objects.filter(cancelled=True, cancelled_approved=False).using('sld')
    sld_cancelled_subscriptions_dict = {}

    for subs in sld_cancelled_subscriptions:
        sld_cancelled_subscriptions_dict[subs.id] = (subs.sldgeneraluser_set.all()[0].your_firstname,
                                                     subs.sldgeneraluser_set.all()[0].your_secondname,
                                                     str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                                     str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                                     'Cancellation pending',
                                                     )

    ssd_cancelled_subscriptions = SSDStandingorderinformation.objects.filter(cancelled=True, cancelled_approved=False).using('ssd')

    ssd_cancelled_subscriptions_dict = {}
    for subs in ssd_cancelled_subscriptions:
        ssd_cancelled_subscriptions_dict[subs.id] = (subs.ssdgeneraluser_set.all()[0].your_firstname,
                                                     subs.ssdgeneraluser_set.all()[0].your_secondname,
                                                     str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                                     str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                                     'Cancellation pending',
                                                     )

    sld_subscriptions = SLDStandingorderinformation.objects.exclude(cancelled=True, cancelled_approved=False).using('sld')

    sld_subscriptions_dict = {}
    for subs in sld_subscriptions:
        status  = ''
        if subs.cancelled:
            if subs.cancelled_approved:
                status = 'Cancelled'
            else:
                status = 'Cancelletion pending'

        else:
            if subs.account_actived:
                status = 'active'
            else:
                status = 'Cancelled'

        sld_subscriptions_dict[subs.id] = (subs.sldgeneraluser_set.all()[0].your_firstname,
                                           subs.sldgeneraluser_set.all()[0].your_secondname,
                                           str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                           str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                           status,
                                           )

    ssd_subscriptions = SSDStandingorderinformation.objects.exclude(cancelled=True, cancelled_approved=False).using('ssd')

    ssd_subscriptions_dict = {}
    for subs in ssd_subscriptions:
        status  = ''
        if subs.cancelled:
            if subs.cancelled_approved:
                status = 'Cancelled'
            else:
                status = 'Cancelletion pending'

        else:
            if subs.account_actived:
                status = 'active'
            else:
                status = 'Cancelled'

        ssd_subscriptions_dict[subs.id] = (subs.ssdgeneraluser_set.all()[0].your_firstname,
                                           subs.ssdgeneraluser_set.all()[0].your_secondname,
                                           str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                           str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                           status,
                                          )




    return render_to_response('backend/subscribers.html',{'sld_cancelled_subscriptions_dict':sld_cancelled_subscriptions_dict,
                                                          'ssd_cancelled_subscriptions_dict':ssd_cancelled_subscriptions_dict,
                                                          'sld_subscriptions_dict': sld_subscriptions_dict,
                                                          'ssd_subscriptions_dict': ssd_subscriptions_dict},context_instance = RequestContext(request))

@gzip_page
@login_required
def messages(request):
    sldmsg = SLDMessage.objects.all().using('sld')
    sldmsg_dict = {}
    for msg in sldmsg:
        status = ''
        if msg.received and msg.is_replied:
            status = 'received (replied to)'
        elif msg.received and not msg.is_replied:
            status = 'received'
        else:
            status = 'sent'

        sldmsg_dict[msg.id] = (msg.user.first_name,msg.user.last_name,status,str(msg.sent.date().today().strftime('%d-%m-%y')),'sld')
    sldpublicmsg = SLDPublicmessage.objects.all().using('sld')
    sldpublicmsg_dict = {}
    for msg in sldpublicmsg:
        status = ''
        if msg.received and msg.is_replied:
            status = 'received (replied to)'
        elif msg.received and not msg.is_replied:
            status = 'received'
        else:
            status = 'sent'
        sldpublicmsg_dict[msg.id] = (msg.user_name.split(' ')[0],msg.user_name.split(' ')[1],status,str(msg.date_sent.date().today().strftime('%d-%m-%y')),'sldp')
    ssdmsg = SSDMessage.objects.all().using('ssd')
    ssdmsg_dict = {}
    for msg in ssdmsg:
        status = ''
        if msg.received and msg.is_replied:
            status = 'received (replied to)'
        elif msg.received and not msg.is_replied:
            status = 'received'
        else:
            status = 'sent'
        ssdmsg_dict[msg.id] = (msg.user.first_name,msg.user.last_name,status,str(msg.sent.date().today().strftime('%d-%m-%y')),'ssd')
    ssdpublicmsg = SSDPublicmessage.objects.all().using('ssd')
    ssdpublicmsg_dict = {}
    for msg in ssdpublicmsg:
        status = ''
        if msg.received and msg.is_replied:
            status = 'received (replied to)'
        elif msg.received and not msg.is_replied:
            status = 'received'
        else:
            status = 'sent'

        ssdpublicmsg_dict[msg.id] = (msg.user_name.split(' ')[0],msg.user_name.split(' ')[1],status,str(msg.date_sent.date().today().strftime('%d-%m-%y')),'ssdp')
    adminmsg = Message.objects.all()
    adminmsg_dict = {}
    for msg in adminmsg:
        status = ''
        if msg.received and msg.is_replied:
            status = 'received (replied to)'
        elif msg.received and not msg.is_replied:
            status = 'received'
        else:
            status = 'sent'
        adminmsg_dict[msg.id] = (msg.user_name.split(' ')[0],msg.user_name.split(' ')[1],status,str(msg.date_sent.date().today().strftime('%d-%m-%y')),'admin')
    return render_to_response('backend/messages.html',{'sldmsg_dict':sldmsg_dict,'sldpublicmsg_dict':sldpublicmsg_dict,'ssdmsg_dict':ssdmsg_dict,'ssdpublicmsg_dict':ssdpublicmsg_dict,'adminmsg_dict':adminmsg_dict},context_instance = RequestContext(request))

@gzip_page
@login_required
def users(request):
    return render_to_response('backend/users.html',context_instance = RequestContext(request))

@login_required
def logout_view(request):
    logout(request)
    return HttpResponseRedirect("/")

@login_required
def subs_popup(request,subs_id,db):
    if db == 'sld':
        soi = SLDStandingorderinformation.objects.all().using('sld').get(id=subs_id)
        email_address = soi.sldgeneraluser_set.all()[0].email
        last_billed = str(soi.last_success_rebilling_date.date().today().strftime('%d-%m-%y'))
        next_billed = str(soi.next_rebilling.date().today().strftime('%d-%m-%y'))
        last_order = str(soi.order_date.date().today().strftime('%d-%m-%y'))
        next_order = str(soi.next_delivery_printed.date().today().strftime('%d-%m-%y'))
        if(soi.unsubscribe_approved):
            cancellation_available = str(soi.unsubscribe_approved.date().today().strftime('%d-%m-%y'))
        else:
            cancellation_available = "Unavailable"

        show_cancel_button = True
        if soi.cancelled and soi.cancelled_approved:
            show_cancel_button = False
        database = 'sld'
        plan_name = 'Save 92% smoking liquid plan'
        delivery_address = soi.sldgeneraluser_set.all()[0].delivery_streetaddress
        billing_address = soi.sldgeneraluser_set.all()[0].billing_streetaddress
    else:
        soi = SSDStandingorderinformation.objects.all().using('ssd').get(id=subs_id)
        email_address = soi.ssdgeneraluser_set.all()[0].email
        last_billed = str(soi.last_success_rebilling_date.date().today().strftime('%d-%m-%y'))
        next_billed = str(soi.next_rebilling.date().today().strftime('%d-%m-%y'))
        last_order = str(soi.order_date.date().today().strftime('%d-%m-%y'))
        next_order = str(soi.next_delivery_printed.date().today().strftime('%d-%m-%y'))
        if(soi.unsubscribe_approved):
            cancellation_available = str(soi.unsubscribe_approved.date().today().strftime('%d-%m-%y'))
        else:
            cancellation_available = "Unavailable"
        show_cancel_button = True
        if soi.cancelled and soi.cancelled_approved:
            show_cancel_button = False
        database = 'ssd'
        plan_name = 'Stop smoking two year plan'
        delivery_address = soi.ssdgeneraluser_set.all()[0].delivery_streetaddress
        billing_address = soi.ssdgeneraluser_set.all()[0].billing_streetaddress

    return render_to_response('backend/subscription_popup.html',{'id' : soi.id,'database':database,'plan_name':plan_name,'email_address':email_address,'last_billed':last_billed,'next_billed':next_billed,'last_order':last_order,'next_order':next_order,'cancellation_available':cancellation_available,'show_cancel_button':show_cancel_button, 'delivery_address': delivery_address, 'billing_address': billing_address},context_instance = RequestContext(request))

@login_required
def autorize_cancellation(request,subs_id,db):
    if db == 'sld':
        soi = SLDStandingorderinformation.objects.all().using('sld').get(id=subs_id)
        soi.cancelled = True
        soi.cancelled_approved = True
        soi.unsubscribe_approved = datetime.datetime.now()
        soi.account_actived = False
        soi.save()
    else:
        soi = SSDStandingorderinformation.objects.all().using('ssd').get(id=subs_id)
        soi.cancelled = True
        soi.cancelled_approved = True
        soi.unsubscribe_approved = datetime.datetime.now()
        soi.account_actived = False
        soi.save()
        data = {'success':True}

    sld_cancelled_subscriptions = SLDStandingorderinformation.objects.filter(cancelled=True, cancelled_approved=False).using('sld')
    sld_cancelled_subscriptions_dict = {}

    for subs in sld_cancelled_subscriptions:
        sld_cancelled_subscriptions_dict[subs.id] = (subs.sldgeneraluser_set.all()[0].your_firstname,
                                                     subs.sldgeneraluser_set.all()[0].your_secondname,
                                                     str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                                     str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                                     'Cancellation pending',
                                                     )

    ssd_cancelled_subscriptions = SSDStandingorderinformation.objects.filter(cancelled=True, cancelled_approved=False).using('ssd')

    ssd_cancelled_subscriptions_dict = {}
    for subs in ssd_cancelled_subscriptions:
        ssd_cancelled_subscriptions_dict[subs.id] = (subs.ssdgeneraluser_set.all()[0].your_firstname,
                                                     subs.ssdgeneraluser_set.all()[0].your_secondname,
                                                     str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                                     str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                                     'Cancellation pending',
                                                     )

    sld_subscriptions = SLDStandingorderinformation.objects.exclude(cancelled=True, cancelled_approved=False).using('sld')

    sld_subscriptions_dict = {}
    for subs in sld_subscriptions:
        status  = ''
        if subs.cancelled:
            if subs.cancelled_approved:
                status = 'Cancelled'
            else:
                status = 'Cancelletion pending'

        else:
            if subs.account_actived:
                status = 'active'
            else:
                status = 'Cancelled'

        sld_subscriptions_dict[subs.id] = (subs.sldgeneraluser_set.all()[0].your_firstname,
                                           subs.sldgeneraluser_set.all()[0].your_secondname,
                                           str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                           str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                           status,
                                           )

    ssd_subscriptions = SSDStandingorderinformation.objects.exclude(cancelled=True, cancelled_approved=False).using('ssd')

    ssd_subscriptions_dict = {}
    for subs in ssd_subscriptions:
        status  = ''
        if subs.cancelled:
            if subs.cancelled_approved:
                status = 'Cancelled'
            else:
                status = 'Cancelletion pending'

        else:
            if subs.account_actived:
                status = 'active'
            else:
                status = 'Cancelled'

        ssd_subscriptions_dict[subs.id] = (subs.ssdgeneraluser_set.all()[0].your_firstname,
                                           subs.ssdgeneraluser_set.all()[0].your_secondname,
                                           str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                           str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                           status,
                                          )



    to_value = request.user.email
    subject_value = 'Order cancelled'
    message_value = """
    Hi

    Were sorry to hear that your leaving us.

    Please accept this email as our acknowledgment that your account will no longer be charged.

    If your thinking of stopping smoking why not visit our other site at stopsmokingthe2yearplan.co.uk

    many thanks

    All the team
    lambertsmokingliquid.co.uk"""

    if settings.USE_SSL:
        try:
            smt = smtplib.SMTP_SSL(host=settings.EMAIL_HOST,port=settings.EMAIL_PORT)
            message = EmailMessage(subject_value,message_value,from_email=settings.EMAIL_HOST_USER,to=to_value)
            smt.sendmail(settings.EMAIL_HOST_USER,to_value,message.message().as_string())
            smt.close()
        except Exception:
            return render_to_response('backend/cancel_subscriber.html',{'sld_cancelled_subscriptions_dict':sld_cancelled_subscriptions_dict,
                                                              'ssd_cancelled_subscriptions_dict':ssd_cancelled_subscriptions_dict,
                                                              'sld_subscriptions_dict': sld_subscriptions_dict,
                                                              'ssd_subscriptions_dict': ssd_subscriptions_dict},context_instance = RequestContext(request))
    else:
        try:
            send_mail(subject_value, message_value, settings.FROM_USER,to_value)
        except Exception:
            return render_to_response('backend/cancel_subscriber.html',{'sld_cancelled_subscriptions_dict':sld_cancelled_subscriptions_dict,
                                                              'ssd_cancelled_subscriptions_dict':ssd_cancelled_subscriptions_dict,
                                                              'sld_subscriptions_dict': sld_subscriptions_dict,
                                                              'ssd_subscriptions_dict': ssd_subscriptions_dict},context_instance = RequestContext(request))

    return render_to_response('backend/cancel_subscriber.html',{'sld_cancelled_subscriptions_dict':sld_cancelled_subscriptions_dict,
                                                          'ssd_cancelled_subscriptions_dict':ssd_cancelled_subscriptions_dict,
                                                          'sld_subscriptions_dict': sld_subscriptions_dict,
                                                          'ssd_subscriptions_dict': ssd_subscriptions_dict},context_instance = RequestContext(request))

@login_required
def print_batch(request,batch_id,db):
    printed_batch = None
    sldprinted_batch = None
    ssdprinted_batch = None
    if db == 'sld':
        printed_batch = SLDPrintBatch.objects.filter(batch_id=batch_id).using('sld')
    elif db == 'ssd':
        printed_batch = SSDPrintBatch.objects.filter(batch_id=batch_id).using('ssd')
    else:
        sldprinted_batch = SLDPrintBatch.objects.filter(batch_id=batch_id).using('sld')
        ssdprinted_batch = SSDPrintBatch.objects.filter(batch_id=batch_id).using('ssd')


    if printed_batch:
        printed_batch = printed_batch[0]
        printed_batch.pending = False
        printed_batch.save()
        if db == 'sld':
            dir_name = os.path.join(settings.MEDIA_ROOT,'temp/%s'%(str(printed_batch.batch_id))) + '_' + str(uuid.uuid4()).replace('-','')
            os.mkdir(dir_name)
            for printed_order in printed_batch.sldprintedorders_set.all():
                printed_order.printed = True
                printed_order.printed_on = datetime.datetime.now()
                printed_order.save()
                order_batch = printed_order.printed_batch.batch_id
                order_number = str(printed_order.order_id)
                generated_on = str(datetime.date.today().strftime('%d-%m-%y'))
                printed_on = str(datetime.date.today().strftime('%d-%m-%y'))

                cor_order = Orderr.objects.all().using('cor').get(tran_id=printed_order.order_id)
                i = 1
                product_list = {}
                for item in cor_order.item_set.all():
                    product_list[i] = (item.supplier_item_name,'1','1','____')
                    i += 1

                for j in range(i,14):
                    product_list[j] = ("-",'-','-','-')

                temp = get_template("backend/print_order.html")
                cont = Context({'order_batch':order_batch,
                                'order_number':order_number,
                                'generated_on': generated_on,
                                'printed_on': printed_on,
                                'product_list':product_list,
                                'checked_by': request.user.first_name + ' ' + request.user.last_name
                                })
                html = temp.render(cont)
                pdf_file_name = order_number
                pdf_file = file(os.path.join(dir_name,'%s.pdf'%(pdf_file_name)),'wb');
                pdf  = pisa.CreatePDF(html,pdf_file)
                pdf_file.close()

            files = os.listdir(dir_name)
            os.chdir(dir_name)
            zfilename = "%s.zip"%(str(printed_batch.batch_id))
            tFile = zipfile.ZipFile(zfilename, "w")
            for f in files:
                if os.path.isfile(f):
                    if file != zfilename:  # para que no meta el archivo comprimido en el archivo comprimido generando un bucle infinito
                        tFile.write(f)
            tFile.close()

            os.chdir("../..")

            rtFile = open(os.path.join(dir_name,zfilename), "r")
            content = rtFile.read()
            rtFile.close()
            iostr = StringIO.StringIO()
            iostr.write(content)

            response = HttpResponse(mimetype='application/zip')
            response['Content-Disposition'] = 'attachment; filename=%s'%(zfilename)
            response.write(iostr.getvalue())

            return response

        elif db == 'ssd':
            printed_batch.pending = False
            printed_batch.save()
            dir_name = os.path.join(settings.MEDIA_ROOT,'temp/%s'%(str(printed_batch.batch_id))) + '_' + str(uuid.uuid4()).replace('-','')
            os.mkdir(dir_name)
            for printed_order in printed_batch.ssdprintedorders_set.all():
                printed_order.printed = True
                printed_order.printed_on = datetime.datetime.now()
                printed_order.save()
                order_batch = printed_order.printed_batch.batch_id
                order_number = str(printed_order.order_id)
                generated_on = str(datetime.date.today().strftime('%d-%m-%y'))
                printed_on = str(datetime.date.today().strftime('%d-%m-%y'))

                cor_order = Orderr.objects.all().using('cor').get(tran_id=printed_order.order_id)
                i = 1
                product_list = {}
                for item in cor_order.item_set.all():
                    product_list[i] = (item.supplier_item_name,str(item.supplier_item_number),'1','____')
                    i += 1

                for j in range(i,14):
                    product_list[j] = ("-",'-','-','-')

                temp = get_template("backend/print_order.html")
                cont = Context({'order_batch':order_batch,
                                'order_number':order_number,
                                'generated_on': generated_on,
                                'printed_on': printed_on,
                                'product_list':product_list,
                                'checked_by': request.user.first_name + ' ' + request.user.last_name
                                })
                html = temp.render(cont)
                pdf_file_name = order_number
                pdf_file = file(os.path.join(settings.MEDIA_ROOT,'temp/%s.pdf'%(pdf_file_name)),'wb');
                pdf  = pisa.CreatePDF(html,pdf_file)
                pdf_file.close()


            files = os.listdir(dir_name)
            os.chdir(dir_name)
            zfilename = "%s.zip"%(str(printed_batch.batch_id))
            tFile = zipfile.ZipFile(zfilename, "w")
            for f in files:
                if os.path.isfile(f):
                    if f != zfilename:  # para que no meta el archivo comprimido en el archivo comprimido generando un bucle infinito
                        tFile.write(f)
            tFile.close()

            os.chdir("../..")

            rtFile = open(dir_name, "r")
            content = rtFile.read()
            rtFile.close()
            iostr = StringIO.StringIO()
            iostr.write(content)

            response = HttpResponse(mimetype='application/zip')
            response['Content-Disposition'] = 'attachment; filename=%s'%(zfilename)
            response.write(iostr.getvalue())

            return response

    elif ssdprinted_batch or sldprinted_batch:
        sldprinted_batch[0].pending = False
        sldprinted_batch[0].save()
        dir_name = os.path.join(settings.MEDIA_ROOT,'temp/%s'%(str(sldprinted_batch[0].batch_id))) + '_' + str(uuid.uuid4()).replace('-','')
        os.mkdir(dir_name)
        for printed_order in sldprinted_batch[0].sldprintedorders_set.all():
            printed_order.printed = True
            printed_order.printed_on = datetime.datetime.now()
            printed_order.save()
            order_batch = printed_order.printed_batch.batch_id
            order_number = str(printed_order.order_id)
            generated_on = str(datetime.date.today().strftime('%d-%m-%y'))
            printed_on = str(datetime.date.today().strftime('%d-%m-%y'))

            cor_order = Orderr.objects.all().using('cor').get(tran_id=printed_order.order_id)
            i = 1
            product_list = {}
            for item in cor_order.item_set.all():
                product_list[i] = (item.supplier_item_name,'1','1','____')
                i += 1

            for j in range(i,14):
                product_list[j] = ("-",'-','-','-')

            temp = get_template("backend/print_order.html")
            cont = Context({'order_batch':order_batch,
                            'order_number':order_number,
                            'generated_on': generated_on,
                            'printed_on': printed_on,
                            'product_list':product_list,
                            'checked_by': request.user.first_name + ' ' + request.user.last_name
                            })
            html = temp.render(cont)
            pdf_file_name = order_number
            pdf_file = file(os.path.join(dir_name,'%s.pdf'%(pdf_file_name)),'wb');
            pdf  = pisa.CreatePDF(html,pdf_file)
            pdf_file.close()

        ssdprinted_batch[0].pending = False
        ssdprinted_batch[0].save()
        for printed_order in ssdprinted_batch[0].ssdprintedorders_set.all():
            printed_order.printed = True
            printed_order.printed_on = datetime.datetime.now()
            printed_order.save()
            order_batch = printed_order.printed_batch.batch_id
            order_number = str(printed_order.order_id)
            generated_on = str(datetime.date.today().strftime('%d-%m-%y'))
            printed_on = str(datetime.date.today().strftime('%d-%m-%y'))

            cor_order = Orderr.objects.all().using('cor').get(tran_id=printed_order.order_id)
            i = 1
            product_list = {}
            for item in cor_order.item_set.all():
                product_list[i] = (item.supplier_item_name,str(item.supplier_item_number),'1','____')
                i += 1

            for j in range(i,14):
                product_list[j] = ("-",'-','-','-')

            temp = get_template("backend/print_order.html")
            cont = Context({'order_batch':order_batch,
                            'order_number':order_number,
                            'generated_on': generated_on,
                            'printed_on': printed_on,
                            'product_list':product_list,
                            'checked_by': request.user.first_name + ' ' + request.user.last_name
                            })
            html = temp.render(cont)
            pdf_file_name = order_number
            pdf_file = file(os.path.join(settings.MEDIA_ROOT,'temp/%s.pdf'%(pdf_file_name)),'wb');
            pdf  = pisa.CreatePDF(html,pdf_file)
            pdf_file.close()

        files = os.listdir(dir_name)
        os.chdir(dir_name)
        zfilename = "%s.zip"%(str(sldprinted_batch[0].batch_id))
        tFile = zipfile.ZipFile(zfilename, "w")
        for f in files:
            if os.path.isfile(f):
                if f != zfilename:  # para que no meta el archivo comprimido en el archivo comprimido generando un bucle infinito
                    tFile.write(f)
        tFile.close()

        os.chdir("../..")

        rtFile = open(dir_name, "r")
        content = rtFile.read()
        rtFile.close()
        iostr = StringIO.StringIO()
        iostr.write(content)

        response = HttpResponse(mimetype='application/zip')
        response['Content-Disposition'] = 'attachment; filename=%s'%(zfilename)
        response.write(iostr.getvalue())

        return response
    else:
        return HttpResponseRedirect('/ouradmin/orders/')


def print_order(request,order_id,db):
    if db=='sld':
        printed_order = SLDPrintedorders.objects.filter(id=order_id).using('sld')
    else:
        printed_order = SSDPrintedorders.objects.filter(id=order_id).using('ssd')

    if printed_order:
        printed_order = printed_order[0]
        printed_order.printed = True
        printed_order.printed_on = datetime.datetime.now()
        printed_order.save()

        if db == 'sld':
            if(printed_order.printed_batch.sldprintedorders_set.filter(printed=False).count() == 0):
                printed_order.printed_batch.pending = False
                printed_order.printed_batch.save()
        else:
            if(printed_order.printed_batch.ssdprintedorders_set.filter(printed=False).count() == 0):
                printed_order.printed_batch.pending = False
                printed_order.printed_batch.save()

        order_batch = printed_order.printed_batch.batch_id
        order_number = str(printed_order.order_id)
        generated_on = str(datetime.date.today().strftime('%d-%m-%y'))
        printed_on = str(datetime.date.today().strftime('%d-%m-%y'))

        cor_order = Orderr.objects.all().using('cor').get(tran_id=printed_order.order_id)
        i = 1
        product_list = {}
        for item in cor_order.item_set.all():
            product_list[i] = (item.supplier_item_name,str(item.supplier_item_number),'1','______')
            i += 1

        for j in range(i,14):
            product_list[j] = ("-",'-','-','-')

        temp = get_template("backend/print_order.html")
        cont = Context({'order_batch':order_batch,
                        'order_number':order_number,
                        'generated_on': generated_on,
                        'printed_on': printed_on,
                        'product_list':product_list,
                        'checked_by': request.user.first_name + ' ' + request.user.last_name
                        })
        html = temp.render(cont)
        pdf_file_name = order_number
        pdf_file = file(os.path.join(settings.MEDIA_ROOT,'temp/%s.pdf'%(pdf_file_name)),'wb');
        pdf  = pisa.CreatePDF(html,pdf_file)
        pdf_file.close()
        input = PdfFileReader(file(os.path.join(settings.MEDIA_ROOT,'temp/%s.pdf'%(pdf_file_name))))
        output = PdfFileWriter()
        for page in input.pages:
            output.addPage(page)
        buffer = StringIO.StringIO()
        output.write(buffer)
        response = HttpResponse(mimetype='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=%s'%(pdf_file_name)
        response.write(buffer.getvalue())
        return response
    else:
        return HttpResponseRedirect('/ouradmin/orders/')

def print_all_orders(request):
    sldprinted_orders = SLDPrintedorders.objects.filter(printed = False).using('sld')
    ssdprinted_orders = SSDPrintedorders.objects.filter(printed = False).using('ssd')

    SSDPrintBatch.objects.all().using('sld').update(pending=False)
    SSDPrintBatch.objects.all().using('ssd').update(pending=False)

    dir_name = os.path.join(settings.MEDIA_ROOT,'temp/%s'%(str(datetime.datetime.now().toordinal()))) + '_' + str(uuid.uuid4()).replace('-','')
    os.mkdir(dir_name)
    if len(sldprinted_orders) > 0 and len(ssdprinted_orders) > 0:
        for printed_order in sldprinted_orders:
            printed_order.printed = True
            printed_order.printed_on = datetime.datetime.now()
            printed_order.save()
            order_batch = printed_order.printed_batch.batch_id
            order_number = str(printed_order.order_id)
            generated_on = str(datetime.date.today().strftime('%d-%m-%y'))
            printed_on = str(datetime.date.today().strftime('%d-%m-%y'))

            cor_order = Orderr.objects.all().using('cor').get(tran_id=printed_order.order_id)
            i = 1
            product_list = {}
            for item in cor_order.item_set.all():
                product_list[i] = (item.supplier_item_name,'1','1','____')
                i += 1

            for j in range(i,14):
                product_list[j] = ("-",'-','-','-')

            temp = get_template("backend/print_order.html")
            cont = Context({'order_batch':order_batch,
                            'order_number':order_number,
                            'generated_on': generated_on,
                            'printed_on': printed_on,
                            'product_list':product_list,
                            'checked_by': request.user.first_name + ' ' + request.user.last_name
                            })
            html = temp.render(cont)
            pdf_file_name = order_number
            pdf_file = file(os.path.join(dir_name,'%s.pdf'%(pdf_file_name)),'wb');
            pdf  = pisa.CreatePDF(html,pdf_file)
            pdf_file.close()

        for printed_order in ssdprinted_orders:
            printed_order.printed = True
            printed_order.printed_on = datetime.datetime.now()
            printed_order.save()
            order_batch = printed_order.printed_batch.batch_id
            order_number = str(printed_order.order_id)
            generated_on = str(datetime.date.today().strftime('%d-%m-%y'))
            printed_on = str(datetime.date.today().strftime('%d-%m-%y'))

            cor_order = Orderr.objects.all().using('cor').get(tran_id=printed_order.order_id)
            i = 1
            product_list = {}
            for item in cor_order.item_set.all():
                product_list[i] = (item.supplier_item_name,str(item.supplier_item_number),'1','____')
                i += 1

            for j in range(i,14):
                product_list[j] = ("-",'-','-','-')

            temp = get_template("backend/print_order.html")
            cont = Context({'order_batch':order_batch,
                            'order_number':order_number,
                            'generated_on': generated_on,
                            'printed_on': printed_on,
                            'product_list':product_list,
                            'checked_by': request.user.first_name + ' ' + request.user.last_name
                            })
            html = temp.render(cont)
            pdf_file_name = order_number
            pdf_file = file(os.path.join(settings.MEDIA_ROOT,'temp/%s.pdf'%(pdf_file_name)),'wb');
            pdf  = pisa.CreatePDF(html,pdf_file)
            pdf_file.close()

        files = os.listdir(dir_name)
        os.chdir(dir_name)
        zfilename = "%s.zip"%(str(datetime.datetime.now().toordinal()))
        tFile = zipfile.ZipFile(zfilename, "w")
        for f in files:
            if os.path.isfile(f):
                if f != zfilename:  # para que no meta el archivo comprimido en el archivo comprimido generando un bucle infinito
                    tFile.write(f)
        tFile.close()

        os.chdir("../..")

        rtFile = open(dir_name, "r")
        content = rtFile.read()
        rtFile.close()
        iostr = StringIO.StringIO()
        iostr.write(content)

        response = HttpResponse(mimetype='application/zip')
        response['Content-Disposition'] = 'attachment; filename=%s'%(zfilename)
        response.write(iostr.getvalue())

        return response


        return HttpResponse()
    else:
        return HttpResponseRedirect('/ouradmin/orders/')

@login_required
def get_orders_by_batch(request,batch_id):
    batchs = {}

    sld_batch = SLDPrintBatch.objects.filter(batch_id__contains=batch_id).using('sld')
    status = ''
    print_all_pending = False
    for batch in sld_batch:
        if batch.pending == True:
            status = 'PENDING'
            print_all_pending = True
        else:
            status = 'Printed'

        temp_orders = []
        orders = batch.sldprintedorders_set.all()
        for order in orders:
            temp_orders.append((order.id,str(order.order_id),order.standing_order.sldgeneraluser_set.all()[0].your_firstname,order.standing_order.sldgeneraluser_set.all()[0].your_secondname,'sld'))


        batchs[batch.batch_id] = (
            batch.sldprintedorders_set.count(),
            str(batch.created_on),
            status,
            temp_orders,
            'sld',
        )

    ssd_batch = SSDPrintBatch.objects.filter(batch_id__contains=batch_id).using('ssd')

    for batch in ssd_batch:
        temp_orders = []
        for order in batch.ssdprintedorders_set.all():
            temp_orders.append((order.id,str(order.order_id),order.standing_order.ssdgeneraluser_set.all()[0].your_firstname,order.standing_order.ssdgeneraluser_set.all()[0].your_secondname,'ssd'))

        if batchs.has_key(batch.batch_id):
            if batchs[batch.batch_id][2] == 'Printed':
                if batch.pending == True:
                    status = 'PENDING'
                    print_all_pending = True
                else:
                    status = 'Printed'
            else:
                status = 'PENDING'
                print_all_pending = True
            temp_arr = batchs[batch.batch_id][3]
            for order in temp_orders:
                 temp_arr.append(order)
            temp_t = (str(int(batchs[batch.batch_id][0]) + batch.ssdprintedorders_set.count()), batchs[batch.batch_id][1],status,temp_arr,'both')
            batchs[batch.batch_id] = temp_t
        else:
            if batch.pending == True:
                    status = 'PENDING'
                    print_all_pending = True
            else:
                status = 'Printed'
            batchs[batch.batch_id] = (
                batch.ssdprintedorders_set.count(),
                str(batch.created_on),
                status,
                temp_orders,
                'ssd'
            )

    return render_to_response('backend/filter_order.html',{'batchs':batchs,'print_all_pending':print_all_pending},context_instance = RequestContext(request))


@login_required
def get_orders_by_number(request,order_number):
    batchs = {}

    sld_batch = SLDPrintBatch.objects.all().using('sld')
    status = ''
    print_all_pending = False
    for batch in sld_batch:
        if batch.pending == True:
            status = 'PENDING'
            print_all_pending = True
        else:
            status = 'Printed'

        temp_orders = []
        orders = batch.sldprintedorders_set.filter(order_id=order_number)
        for order in orders:
            temp_orders.append((order.id,str(order.order_id),order.standing_order.sldgeneraluser_set.all()[0].your_firstname,order.standing_order.sldgeneraluser_set.all()[0].your_secondname,'sld'))

        if len(orders) > 0:
            batchs[batch.batch_id] = (
                batch.sldprintedorders_set.count(),
                str(batch.created_on),
                status,
                temp_orders,
                'sld',
            )

    ssd_batch = SSDPrintBatch.objects.all().using('ssd')

    for batch in ssd_batch:
        temp_orders = []
        for order in batch.ssdprintedorders_set.filter(order_id=order_number):
            temp_orders.append((order.id,str(order.order_id),order.standing_order.ssdgeneraluser_set.all()[0].your_firstname,order.standing_order.ssdgeneraluser_set.all()[0].your_secondname,'ssd'))

        if len(temp_orders) > 0:
            if batchs.has_key(batch.batch_id):
                if batchs[batch.batch_id][2] == 'Printed':
                    if batch.pending == True:
                        status = 'PENDING'
                        print_all_pending = True
                    else:
                        status = 'Printed'
                else:
                    status = 'PENDING'
                    print_all_pending = True
                temp_arr = batchs[batch.batch_id][3]
                for order in temp_orders:
                     temp_arr.append(order)
                temp_t = (str(int(batchs[batch.batch_id][0]) + batch.ssdprintedorders_set.count()), batchs[batch.batch_id][1],status,temp_arr,'both')
                batchs[batch.batch_id] = temp_t
            else:
                if batch.pending == True:
                        status = 'PENDING'
                        print_all_pending = True
                else:
                    status = 'Printed'
                batchs[batch.batch_id] = (
                    batch.ssdprintedorders_set.count(),
                    str(batch.created_on),
                    status,
                    temp_orders,
                    'ssd'
                )

    return render_to_response('backend/filter_order.html',{'batchs':batchs,'print_all_pending':print_all_pending},context_instance = RequestContext(request))


def get_orders(request):
    batchs = {}

    sld_batch = SLDPrintBatch.objects.all().using('sld')
    status = ''
    print_all_pending = False
    for batch in sld_batch:
        if batch.pending == True:
            status = 'PENDING'
            print_all_pending = True
        else:
            status = 'Printed'


        temp_orders = []
        orders = batch.sldprintedorders_set.all()
        for order in orders:
            temp_orders.append((order.id,str(order.order_id),order.standing_order.sldgeneraluser_set.all()[0].your_firstname,order.standing_order.sldgeneraluser_set.all()[0].your_secondname,'sld'))


        batchs[batch.batch_id] = (
            batch.sldprintedorders_set.count(),
            str(batch.created_on),
            status,
            temp_orders,
            'sld',
        )

    ssd_batch = SSDPrintBatch.objects.all().using('ssd')

    for batch in ssd_batch:
        temp_orders = []
        for order in batch.ssdprintedorders_set.all():
            temp_orders.append((order.id,str(order.order_id),order.standing_order.ssdgeneraluser_set.all()[0].your_firstname,order.standing_order.ssdgeneraluser_set.all()[0].your_secondname,'ssd'))

        if batchs.has_key(batch.batch_id):
            if batchs[batch.batch_id][2] == 'Printed':
                if batch.pending == True:
                    status = 'PENDING'
                    print_all_pending = True
                else:
                    status = 'Printed'
            else:
                status = 'PENDING'
                print_all_pending = True
            temp_arr = batchs[batch.batch_id][3]
            for order in temp_orders:
                 temp_arr.append(order)
            temp_t = (str(int(batchs[batch.batch_id][0]) + batch.ssdprintedorders_set.count()), batchs[batch.batch_id][1],status,temp_arr,'both')
            batchs[batch.batch_id] = temp_t
        else:
            if batch.pending == True:
                    status = 'PENDING'
                    print_all_pending = True
            else:
                status = 'Printed'
            batchs[batch.batch_id] = (
                batch.ssdprintedorders_set.count(),
                str(batch.created_on),
                status,
                temp_orders,
                'ssd'
            )

    return render_to_response('backend/filter_order.html',{'batchs':batchs,'print_all_pending':print_all_pending},context_instance = RequestContext(request))



@login_required
def get_pending_orders(request):
    batchs = {}

    sld_batch = SLDPrintBatch.objects.all().using('sld')
    status = ''
    print_all_pending = False
    for batch in sld_batch:
        if batch.pending == True:
            status = 'PENDING'
            print_all_pending = True
        else:
            status = 'Printed'

        temp_orders = []
        orders = batch.sldprintedorders_set.filter(printed=False)
        for order in orders:
            temp_orders.append((order.id,str(order.order_id),order.standing_order.sldgeneraluser_set.all()[0].your_firstname,order.standing_order.sldgeneraluser_set.all()[0].your_secondname,'sld'))

        if len(orders) > 0:
            batchs[batch.batch_id] = (
                batch.sldprintedorders_set.count(),
                str(batch.created_on),
                status,
                temp_orders,
                'sld',
            )

    ssd_batch = SSDPrintBatch.objects.all().using('ssd')

    for batch in ssd_batch:
        temp_orders = []
        for order in batch.ssdprintedorders_set.filter(printed=False):
            temp_orders.append((order.id,str(order.order_id),order.standing_order.ssdgeneraluser_set.all()[0].your_firstname,order.standing_order.ssdgeneraluser_set.all()[0].your_secondname,'ssd'))

        if len(temp_orders) > 0:
            if batchs.has_key(batch.batch_id):
                if batchs[batch.batch_id][2] == 'Printed':
                    if batch.pending == True:
                        status = 'PENDING'
                        print_all_pending = True
                    else:
                        status = 'Printed'
                else:
                    status = 'PENDING'
                    print_all_pending = True
                temp_arr = batchs[batch.batch_id][3]
                for order in temp_orders:
                     temp_arr.append(order)
                temp_t = (str(int(batchs[batch.batch_id][0]) + batch.ssdprintedorders_set.count()), batchs[batch.batch_id][1],status,temp_arr,'both')
                batchs[batch.batch_id] = temp_t
            else:
                if batch.pending == True:
                        status = 'PENDING'
                        print_all_pending = True
                else:
                    status = 'Printed'
                batchs[batch.batch_id] = (
                    batch.ssdprintedorders_set.count(),
                    str(batch.created_on),
                    status,
                    temp_orders,
                    'ssd'
                )

    return render_to_response('backend/filter_order.html',{'batchs':batchs,'print_all_pending':print_all_pending},context_instance = RequestContext(request))

@gzip_page
@login_required
def print_order_as_html(request,order_id,db):
    if db=='sld':
        printed_order = SLDPrintedorders.objects.filter(id=order_id).using('sld')
    else:
        printed_order = SSDPrintedorders.objects.filter(id=order_id).using('ssd')

    if printed_order:
        printed_order = printed_order[0]
        printed_order.printed = True
        printed_order.printed_on = datetime.datetime.now()
        printed_order.save()

        if db == 'sld':
            if(printed_order.printed_batch.sldprintedorders_set.filter(printed=False).count() == 0):
                printed_order.printed_batch.pending = False
                printed_order.printed_batch.save()
        else:
            if(printed_order.printed_batch.ssdprintedorders_set.filter(printed=False).count() == 0):
                printed_order.printed_batch.pending = False
                printed_order.printed_batch.save()

        order_batch = printed_order.printed_batch.batch_id
        order_number = str(printed_order.order_id)
        generated_on = str(datetime.date.today().strftime('%d-%m-%y'))
        printed_on = str(datetime.date.today().strftime('%d-%m-%y'))

        cor_order = Orderr.objects.all().using('cor').get(tran_id=printed_order.order_id)
        i = 1
        product_list = {}
        for item in cor_order.item_set.all():
            product_list[i] = (item.supplier_item_name,str(item.supplier_item_number),'1','______')
            i += 1

        for j in range(i,14):
            product_list[j] = ("-",'-','-','-')


        return render_to_response('backend/print_order_view.html',{'order_batch':order_batch,
                        'order_number':order_number,
                        'generated_on': generated_on,
                        'printed_on': printed_on,
                        'product_list':product_list,
                        },context_instance = RequestContext(request))

    else:
        return HttpResponseRedirect('/ouradmin/orders/')

@gzip_page
@login_required
def print_batch_as_html(request,batch_id,db):
    printed_batch = None
    sldprinted_batch = None
    ssdprinted_batch = None
    if db == 'sld':
        printed_batch = SLDPrintBatch.objects.filter(batch_id=batch_id).using('sld')
    elif db == 'ssd':
        printed_batch = SSDPrintBatch.objects.filter(batch_id=batch_id).using('ssd')
    else:
        sldprinted_batch = SLDPrintBatch.objects.filter(batch_id=batch_id).using('sld')
        ssdprinted_batch = SSDPrintBatch.objects.filter(batch_id=batch_id).using('ssd')


    if printed_batch:
        printed_batch = printed_batch[0]
        printed_batch.pending = False
        printed_batch.save()
        if db == 'sld':
            batch_list = []
            for printed_order in printed_batch.sldprintedorders_set.all():
                printed_order.printed = True
                printed_order.printed_on = datetime.datetime.now()
                printed_order.save()
                order_batch = printed_order.printed_batch.batch_id
                order_number = str(printed_order.order_id)
                generated_on = str(datetime.date.today().strftime('%d-%m-%y'))
                printed_on = str(datetime.date.today().strftime('%d-%m-%y'))

                cor_order = Orderr.objects.all().using('cor').get(tran_id=printed_order.order_id)
                i = 1
                product_list = []
                for item in cor_order.item_set.all():
                    product_list.append((str(i),item.supplier_item_name,'1','1','____'))
                    i += 1

                for j in range(i,14):
                    product_list.append((str(j),"-",'-','-','-'))


                batch_list.append((order_batch,order_number,generated_on,printed_on,product_list,))


            return render_to_response('backend/print_batch_view.html',{'batch_list':batch_list},context_instance = RequestContext(request))

        elif db == 'ssd':
            printed_batch.pending = False
            printed_batch.save()
            batch_list = []
            for printed_order in printed_batch.ssdprintedorders_set.all():
                printed_order.printed = True
                printed_order.printed_on = datetime.datetime.now()
                printed_order.save()
                order_batch = printed_order.printed_batch.batch_id
                order_number = str(printed_order.order_id)
                generated_on = str(datetime.date.today().strftime('%d-%m-%y'))
                printed_on = str(datetime.date.today().strftime('%d-%m-%y'))

                cor_order = Orderr.objects.all().using('cor').get(tran_id=printed_order.order_id)
                i = 1
                product_list = []
                for item in cor_order.item_set.all():
                    product_list.append((str(i),item.supplier_item_name,str(item.supplier_item_number),'1','____'))
                    i += 1

                for j in range(i,14):
                    product_list.append((str(j),"-",'-','-','-'))


                batch_list.append((order_batch,order_number,generated_on,printed_on,product_list,))

            return render_to_response('backend/print_batch_view.html',{'batch_list':batch_list},context_instance = RequestContext(request))

    elif ssdprinted_batch or sldprinted_batch:
        sldprinted_batch[0].pending = False
        sldprinted_batch[0].save()
        batch_list = []
        for printed_order in sldprinted_batch[0].sldprintedorders_set.all():
            printed_order.printed = True
            printed_order.printed_on = datetime.datetime.now()
            printed_order.save()
            order_batch = printed_order.printed_batch.batch_id
            order_number = str(printed_order.order_id)
            generated_on = str(datetime.date.today().strftime('%d-%m-%y'))
            printed_on = str(datetime.date.today().strftime('%d-%m-%y'))

            cor_order = Orderr.objects.all().using('cor').get(tran_id=printed_order.order_id)
            i = 1
            product_list = []
            for item in cor_order.item_set.all():
                product_list.append((str(i),item.supplier_item_name,'1','1','____'))
                i += 1

            for j in range(i,14):
                product_list.append((str(j),"-",'-','-','-'))


            batch_list.append((order_batch,order_number,generated_on,printed_on,product_list,))

        ssdprinted_batch[0].pending = False
        ssdprinted_batch[0].save()
        for printed_order in ssdprinted_batch[0].ssdprintedorders_set.all():
            printed_order.printed = True
            printed_order.printed_on = datetime.datetime.now()
            printed_order.save()
            order_batch = printed_order.printed_batch.batch_id
            order_number = str(printed_order.order_id)
            generated_on = str(datetime.date.today().strftime('%d-%m-%y'))
            printed_on = str(datetime.date.today().strftime('%d-%m-%y'))

            cor_order = Orderr.objects.all().using('cor').get(tran_id=printed_order.order_id)
            i = 1
            product_list = []
            for item in cor_order.item_set.all():
                product_list.append((str(i),item.supplier_item_name,str(item.supplier_item_number),'1','____'))
                i += 1

            for j in range(i,14):
                product_list.append((str(j),"-",'-','-','-'))

            batch_list.append((order_batch,order_number,generated_on,printed_on,product_list,))


        return render_to_response('backend/print_batch_view.html',{'batch_list':batch_list},context_instance = RequestContext(request))
    else:
        return HttpResponseRedirect('/ouradmin/orders/')

@gzip_page
@login_required
def print_all_orders_as_html(request):
    sldprinted_orders = SLDPrintedorders.objects.filter(printed = False).using('sld')
    ssdprinted_orders = SSDPrintedorders.objects.filter(printed = False).using('ssd')

    SSDPrintBatch.objects.all().using('sld').update(pending=False)
    SSDPrintBatch.objects.all().using('ssd').update(pending=False)

    batch_list = []
    if len(sldprinted_orders) > 0 and len(ssdprinted_orders) > 0:
        for printed_order in sldprinted_orders:
            printed_order.printed = True
            printed_order.printed_on = datetime.datetime.now()
            printed_order.save()
            order_batch = printed_order.printed_batch.batch_id
            order_number = str(printed_order.order_id)
            generated_on = str(datetime.date.today().strftime('%d-%m-%y'))
            printed_on = str(datetime.date.today().strftime('%d-%m-%y'))

            cor_order = Orderr.objects.all().using('cor').get(tran_id=printed_order.order_id)
            i = 1
            product_list = []
            for item in cor_order.item_set.all():
                product_list.append((str(i),item.supplier_item_name,'1','1','____'))
                i += 1

            for j in range(i,14):
                product_list.append((str(j),"-",'-','-','-'))

            batch_list.append((order_batch,order_number,generated_on,printed_on,product_list,))

        for printed_order in ssdprinted_orders:
            printed_order.printed = True
            printed_order.printed_on = datetime.datetime.now()
            printed_order.save()
            order_batch = printed_order.printed_batch.batch_id
            order_number = str(printed_order.order_id)
            generated_on = str(datetime.date.today().strftime('%d-%m-%y'))
            printed_on = str(datetime.date.today().strftime('%d-%m-%y'))

            cor_order = Orderr.objects.all().using('cor').get(tran_id=printed_order.order_id)
            i = 1
            product_list = []
            for item in cor_order.item_set.all():
                product_list.append((str(i),item.supplier_item_name,str(item.supplier_item_number),'1','____'))
                i += 1

            for j in range(i,14):
                product_list.append((str(j),"-",'-','-','-'))


            batch_list.append((order_batch,order_number,generated_on,printed_on,product_list,))

        return render_to_response('backend/print_batch_view.html',{'batch_list':batch_list},context_instance = RequestContext(request))
    elif(len(sldprinted_orders) > 0):
        for printed_order in sldprinted_orders:
            printed_order.printed = True
            printed_order.printed_on = datetime.datetime.now()
            printed_order.save()
            order_batch = printed_order.printed_batch.batch_id
            order_number = str(printed_order.order_id)
            generated_on = str(datetime.date.today().strftime('%d-%m-%y'))
            printed_on = str(datetime.date.today().strftime('%d-%m-%y'))

            cor_order = Orderr.objects.all().using('cor').get(tran_id=printed_order.order_id)
            i = 1
            product_list = []
            for item in cor_order.item_set.all():
                product_list.append((str(i),item.supplier_item_name,'1','1','____'))
                i += 1

            for j in range(i,14):
                product_list.append((str(j),"-",'-','-','-'))

            batch_list.append((order_batch,order_number,generated_on,printed_on,product_list,))

        return  render_to_response('backend/print_batch_view.html',{'batch_list':batch_list},context_instance = RequestContext(request))
    elif(len(ssdprinted_orders) > 0):
        for printed_order in ssdprinted_orders:
            printed_order.printed = True
            printed_order.printed_on = datetime.datetime.now()
            printed_order.save()
            order_batch = printed_order.printed_batch.batch_id
            order_number = str(printed_order.order_id)
            generated_on = str(datetime.date.today().strftime('%d-%m-%y'))
            printed_on = str(datetime.date.today().strftime('%d-%m-%y'))

            cor_order = Orderr.objects.all().using('cor').get(tran_id=printed_order.order_id)
            i = 1
            product_list = []
            for item in cor_order.item_set.all():
                product_list.append((str(i),item.supplier_item_name,str(item.supplier_item_number),'1','____'))
                i += 1

            for j in range(i,14):
                product_list.append((str(j),"-",'-','-','-'))


            batch_list.append((order_batch,order_number,generated_on,printed_on,product_list,))

        return render_to_response('backend/print_batch_view.html',{'batch_list':batch_list},context_instance = RequestContext(request))
    else:
        return HttpResponseRedirect('/ouradmin/orders/')

@login_required
def view_order(request,order_id,db):
    if request.is_ajax():
        host_name = request.META['HTTP_HOST']
        if request.is_secure():
            urlpart = 'https://%s'%(host_name)
        else:
            urlpart = 'http://%s'%(host_name)

        batchs = {}

        if db=='sld':
            printed_order = SLDPrintedorders.objects.filter(id=order_id).using('sld')
        else:
            printed_order = SSDPrintedorders.objects.filter(id=order_id).using('ssd')

        if printed_order:
            printed_order = printed_order[0]
            printed_order.printed = True
            printed_order.printed_on = datetime.datetime.now()
            printed_order.save()

            if db == 'sld':
                if(printed_order.printed_batch.sldprintedorders_set.filter(printed=False).count() == 0):
                    printed_order.printed_batch.pending = False
                    printed_order.printed_batch.save()
            else:
                if(printed_order.printed_batch.ssdprintedorders_set.filter(printed=False).count() == 0):
                    printed_order.printed_batch.pending = False
                    printed_order.printed_batch.save()

            order_batch = printed_order.printed_batch.batch_id
            order_number = str(printed_order.order_id)
            generated_on = str(datetime.date.today().strftime('%d-%m-%y'))
            printed_on = str(datetime.date.today().strftime('%d-%m-%y'))

            cor_order = Orderr.objects.all().using('cor').get(tran_id=printed_order.order_id)
            i = 1
            product_list = {}
            for item in cor_order.item_set.all():
                product_list[i] = (item.supplier_item_name,str(item.supplier_item_number),'1','______')
                i += 1

            for j in range(i,14):
                product_list[j] = ("-",'-','-','-')

            temp = get_template("backend/print_order.html")
            cont = Context({'order_batch':order_batch,
                            'order_number':order_number,
                            'generated_on': generated_on,
                            'printed_on': printed_on,
                            'product_list':product_list,
                            })
            html = temp.render(cont)
            pdf_file_name = order_number
            pdf_file = file(os.path.join(settings.MEDIA_ROOT,'temp/%s.pdf'%(pdf_file_name)),'wb');
            pdf  = pisa.CreatePDF(html,pdf_file)
            pdf_file.close()

            strjson = json.dumps({'pdf_path': urlpart + '/media/temp/%s.pdf'%(pdf_file_name)})

            return HttpResponse(strjson, content_type='application/json')
        else:
            return HttpResponseRedirect('/ouradmin/orders/')
    else:
        return HttpResponse()

@login_required
def get_subscriber_by_name(request,filter_name):
    sld_cancelled_subscriptions = SLDStandingorderinformation.objects.filter(cancelled=True, cancelled_approved=False).using('sld')
    sld_cancelled_subscriptions_dict = {}

    for subs in sld_cancelled_subscriptions:
        if subs.sldgeneraluser_set.all()[0].your_firstname.lower().find(filter_name.lower()) != -1 or subs.sldgeneraluser_set.all()[0].your_secondname.lower().find(filter_name.lower()) != -1:
            sld_cancelled_subscriptions_dict[subs.id] = (subs.sldgeneraluser_set.all()[0].your_firstname,
                                                         subs.sldgeneraluser_set.all()[0].your_secondname,
                                                         str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                                         str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                                         'Cancellation pending',
                                                         )

    ssd_cancelled_subscriptions = SSDStandingorderinformation.objects.filter(cancelled=True, cancelled_approved=False).using('ssd')

    ssd_cancelled_subscriptions_dict = {}
    for subs in ssd_cancelled_subscriptions:
        if subs.ssdgeneraluser_set.all()[0].your_firstname.lower().find(filter_name.lower()) != -1 or subs.ssdgeneraluser_set.all()[0].your_secondname.lower().find(filter_name.lower()) != -1:
            ssd_cancelled_subscriptions_dict[subs.id] = (subs.ssdgeneraluser_set.all()[0].your_firstname,
                                                         subs.ssdgeneraluser_set.all()[0].your_secondname,
                                                         str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                                         str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                                         'Cancellation pending',
                                                         )

    sld_subscriptions = SLDStandingorderinformation.objects.exclude(cancelled=True, cancelled_approved=False).using('sld')

    sld_subscriptions_dict = {}
    for subs in sld_subscriptions:
        if subs.sldgeneraluser_set.all()[0].your_firstname.lower().find(filter_name.lower()) != -1 or subs.sldgeneraluser_set.all()[0].your_secondname.lower().find(filter_name.lower()) != -1:
            status  = ''
            if subs.cancelled:
                if subs.cancelled_approved:
                    status = 'Cancelled'
                else:
                    status = 'Cancelletion pending'

            else:
                if subs.account_actived:
                    status = 'active'
                else:
                    status = 'Cancelled'

            sld_subscriptions_dict[subs.id] = (subs.sldgeneraluser_set.all()[0].your_firstname,
                                               subs.sldgeneraluser_set.all()[0].your_secondname,
                                               str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                               str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                               status,
                                               )

    ssd_subscriptions = SSDStandingorderinformation.objects.exclude(cancelled=True, cancelled_approved=False).using('ssd')

    ssd_subscriptions_dict = {}
    for subs in ssd_subscriptions:
        if subs.ssdgeneraluser_set.all()[0].your_firstname.lower().find(filter_name.lower()) != -1 or subs.ssdgeneraluser_set.all()[0].your_secondname.lower().find(filter_name.lower()) != -1:
            status  = ''
            if subs.cancelled:
                if subs.cancelled_approved:
                    status = 'Cancelled'
                else:
                    status = 'Cancelletion pending'

            else:
                if subs.account_actived:
                    status = 'active'
                else:
                    status = 'Cancelled'

            ssd_subscriptions_dict[subs.id] = (subs.ssdgeneraluser_set.all()[0].your_firstname,
                                               subs.ssdgeneraluser_set.all()[0].your_secondname,
                                               str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                               str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                               status,)


    return render_to_response('backend/filter_subscribers.html',{'sld_cancelled_subscriptions_dict':sld_cancelled_subscriptions_dict,
                                                          'ssd_cancelled_subscriptions_dict':ssd_cancelled_subscriptions_dict,
                                                          'sld_subscriptions_dict': sld_subscriptions_dict,
                                                          'ssd_subscriptions_dict': ssd_subscriptions_dict},context_instance = RequestContext(request))


@login_required
def get_subscriber_by_postcode(request,filter_name):
    sld_cancelled_subscriptions = SLDStandingorderinformation.objects.filter(cancelled=True, cancelled_approved=False).using('sld')
    sld_cancelled_subscriptions_dict = {}

    for subs in sld_cancelled_subscriptions:
        if subs.sldgeneraluser_set.all()[0].your_postcode.lower().find(filter_name.lower()) != -1:
            sld_cancelled_subscriptions_dict[subs.id] = (subs.sldgeneraluser_set.all()[0].your_firstname,
                                                         subs.sldgeneraluser_set.all()[0].your_secondname,
                                                         str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                                         str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                                         'Cancellation pending',
                                                         )

    ssd_cancelled_subscriptions = SSDStandingorderinformation.objects.filter(cancelled=True, cancelled_approved=False).using('ssd')

    ssd_cancelled_subscriptions_dict = {}
    for subs in ssd_cancelled_subscriptions:
        if subs.ssdgeneraluser_set.all()[0].your_postcode.lower().find(filter_name.lower()) != -1:
            ssd_cancelled_subscriptions_dict[subs.id] = (subs.ssdgeneraluser_set.all()[0].your_firstname,
                                                         subs.ssdgeneraluser_set.all()[0].your_secondname,
                                                         str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                                         str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                                         'Cancellation pending',
                                                         )

    sld_subscriptions = SLDStandingorderinformation.objects.exclude(cancelled=True, cancelled_approved=False).using('sld')

    sld_subscriptions_dict = {}
    for subs in sld_subscriptions:
        if subs.sldgeneraluser_set.all()[0].your_postcode.lower().find(filter_name.lower()) != -1:
            status  = ''
            if subs.cancelled:
                if subs.cancelled_approved:
                    status = 'Cancelled'
                else:
                    status = 'Cancelletion pending'

            else:
                if subs.account_actived:
                    status = 'active'
                else:
                    status = 'Cancelled'

            sld_subscriptions_dict[subs.id] = (subs.sldgeneraluser_set.all()[0].your_firstname,
                                               subs.sldgeneraluser_set.all()[0].your_secondname,
                                               str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                               str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                               status,
                                               )

    ssd_subscriptions = SSDStandingorderinformation.objects.exclude(cancelled=True, cancelled_approved=False).using('ssd')

    ssd_subscriptions_dict = {}
    for subs in ssd_subscriptions:
        if subs.ssdgeneraluser_set.all()[0].your_postcode.lower().find(filter_name.lower()) != -1:
            status  = ''
            if subs.cancelled:
                if subs.cancelled_approved:
                    status = 'Cancelled'
                else:
                    status = 'Cancelletion pending'

            else:
                if subs.account_actived:
                    status = 'active'
                else:
                    status = 'Cancelled'

            ssd_subscriptions_dict[subs.id] = (subs.ssdgeneraluser_set.all()[0].your_firstname,
                                               subs.ssdgeneraluser_set.all()[0].your_secondname,
                                               str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                               str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                               status,)


    return render_to_response('backend/filter_subscribers.html',{'sld_cancelled_subscriptions_dict':sld_cancelled_subscriptions_dict,
                                                          'ssd_cancelled_subscriptions_dict':ssd_cancelled_subscriptions_dict,
                                                          'sld_subscriptions_dict': sld_subscriptions_dict,
                                                          'ssd_subscriptions_dict': ssd_subscriptions_dict},context_instance = RequestContext(request))


@login_required
def get_subscribers(request):
    sld_cancelled_subscriptions = SLDStandingorderinformation.objects.filter(cancelled=True, cancelled_approved=False).using('sld')
    sld_cancelled_subscriptions_dict = {}

    for subs in sld_cancelled_subscriptions:
        sld_cancelled_subscriptions_dict[subs.id] = (subs.sldgeneraluser_set.all()[0].your_firstname,
                                                     subs.sldgeneraluser_set.all()[0].your_secondname,
                                                     str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                                     str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                                     'Cancellation pending',
                                                     )

    ssd_cancelled_subscriptions = SSDStandingorderinformation.objects.filter(cancelled=True, cancelled_approved=False).using('ssd')

    ssd_cancelled_subscriptions_dict = {}
    for subs in ssd_cancelled_subscriptions:
        ssd_cancelled_subscriptions_dict[subs.id] = (subs.ssdgeneraluser_set.all()[0].your_firstname,
                                                     subs.ssdgeneraluser_set.all()[0].your_secondname,
                                                     str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                                     str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                                     'Cancellation pending',
                                                     )

    sld_subscriptions = SLDStandingorderinformation.objects.exclude(cancelled=True, cancelled_approved=False).using('sld')

    sld_subscriptions_dict = {}
    for subs in sld_subscriptions:
        status  = ''
        if subs.cancelled:
            if subs.cancelled_approved:
                status = 'Cancelled'
            else:
                status = 'Cancelletion pending'

        else:
            if subs.account_actived:
                status = 'active'
            else:
                status = 'Cancelled'

        sld_subscriptions_dict[subs.id] = (subs.sldgeneraluser_set.all()[0].your_firstname,
                                           subs.sldgeneraluser_set.all()[0].your_secondname,
                                           str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                           str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                           status,
                                           )

    ssd_subscriptions = SSDStandingorderinformation.objects.exclude(cancelled=True, cancelled_approved=False).using('ssd')

    ssd_subscriptions_dict = {}
    for subs in ssd_subscriptions:
        status  = ''
        if subs.cancelled:
            if subs.cancelled_approved:
                status = 'Cancelled'
            else:
                status = 'Cancelletion pending'

        else:
            if subs.account_actived:
                status = 'active'
            else:
                status = 'Cancelled'

        ssd_subscriptions_dict[subs.id] = (subs.ssdgeneraluser_set.all()[0].your_firstname,
                                           subs.ssdgeneraluser_set.all()[0].your_secondname,
                                           str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                           str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                           status,)


    return render_to_response('backend/filter_subscribers.html',{'sld_cancelled_subscriptions_dict':sld_cancelled_subscriptions_dict,
                                                          'ssd_cancelled_subscriptions_dict':ssd_cancelled_subscriptions_dict,
                                                          'sld_subscriptions_dict': sld_subscriptions_dict,
                                                          'ssd_subscriptions_dict': ssd_subscriptions_dict},context_instance = RequestContext(request))

@login_required
def get_subscribers_by_status(request, status):
    status = int(status)
    if status == 1:
        sld_subscriptions = SLDStandingorderinformation.objects.filter(cancelled=False).using('sld')

        sld_subscriptions_dict = {}
        for subs in sld_subscriptions:
            status  = ''
            if subs.cancelled:
                if subs.cancelled_approved:
                    status = 'Cancelled'
                else:
                    status = 'Cancelletion pending'

            else:
                if subs.account_actived:
                    status = 'active'
                else:
                    status = 'Cancelled'

            sld_subscriptions_dict[subs.id] = (subs.sldgeneraluser_set.all()[0].your_firstname,
                                               subs.sldgeneraluser_set.all()[0].your_secondname,
                                               str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                               str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                               status,
                                               )

        ssd_subscriptions = SSDStandingorderinformation.objects.filter(cancelled=False).using('ssd')

        ssd_subscriptions_dict = {}
        for subs in ssd_subscriptions:
            status  = ''
            if subs.cancelled:
                if subs.cancelled_approved:
                    status = 'Cancelled'
                else:
                    status = 'Cancelletion pending'

            else:
                if subs.account_actived:
                    status = 'active'
                else:
                    status = 'Cancelled'

            ssd_subscriptions_dict[subs.id] = (subs.ssdgeneraluser_set.all()[0].your_firstname,
                                               subs.ssdgeneraluser_set.all()[0].your_secondname,
                                               str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                               str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                               status,)

        sld_cancelled_subscriptions_dict = {}
        ssd_cancelled_subscriptions_dict = {}
        return render_to_response('backend/filter_subscribers.html',{'sld_cancelled_subscriptions_dict':sld_cancelled_subscriptions_dict,
                                                              'ssd_cancelled_subscriptions_dict':ssd_cancelled_subscriptions_dict,
                                                              'sld_subscriptions_dict': sld_subscriptions_dict,
                                                              'ssd_subscriptions_dict': ssd_subscriptions_dict},context_instance = RequestContext(request))
    elif status == 2:
        sld_cancelled_subscriptions = SLDStandingorderinformation.objects.filter(cancelled=True, cancelled_approved=True).using('sld')
        sld_subscriptions_dict = {}

        for subs in sld_cancelled_subscriptions:
            sld_subscriptions_dict[subs.id] = (subs.sldgeneraluser_set.all()[0].your_firstname,
                                                         subs.sldgeneraluser_set.all()[0].your_secondname,
                                                         str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                                         str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                                         'Cancelled',
                                                         )

        ssd_cancelled_subscriptions = SSDStandingorderinformation.objects.filter(cancelled=True, cancelled_approved=True).using('ssd')

        ssd_subscriptions_dict = {}
        for subs in ssd_cancelled_subscriptions:
            ssd_subscriptions_dict[subs.id] = (subs.ssdgeneraluser_set.all()[0].your_firstname,
                                                         subs.ssdgeneraluser_set.all()[0].your_secondname,
                                                         str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                                         str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                                         'Cancelled',
                                                         )

        sld_cancelled_subscriptions_dict = {}
        ssd_cancelled_subscriptions_dict = {}
        return render_to_response('backend/filter_subscribers.html',{'sld_cancelled_subscriptions_dict':sld_cancelled_subscriptions_dict,
                                                              'ssd_cancelled_subscriptions_dict':ssd_cancelled_subscriptions_dict,
                                                              'sld_subscriptions_dict': sld_subscriptions_dict,
                                                              'ssd_subscriptions_dict': ssd_subscriptions_dict},context_instance = RequestContext(request))
    else:
        sld_cancelled_subscriptions = SLDStandingorderinformation.objects.filter(cancelled=True, cancelled_approved=False).using('sld')
        sld_cancelled_subscriptions_dict = {}

        for subs in sld_cancelled_subscriptions:
            sld_cancelled_subscriptions_dict[subs.id] = (subs.sldgeneraluser_set.all()[0].your_firstname,
                                                         subs.sldgeneraluser_set.all()[0].your_secondname,
                                                         str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                                         str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                                         'Cancellation pending',
                                                         )

        ssd_cancelled_subscriptions = SSDStandingorderinformation.objects.filter(cancelled=True, cancelled_approved=False).using('ssd')

        ssd_cancelled_subscriptions_dict = {}
        for subs in ssd_cancelled_subscriptions:
            ssd_cancelled_subscriptions_dict[subs.id] = (subs.ssdgeneraluser_set.all()[0].your_firstname,
                                                         subs.ssdgeneraluser_set.all()[0].your_secondname,
                                                         str(subs.order_date.date().today().strftime('%d-%m-%y')),
                                                         str(subs.next_delivery_printed.date().today().strftime('%d-%m-%y')),
                                                         'Cancellation pending',
                                                         )

        sld_subscriptions_dict = {}
        ssd_subscriptions_dict = {}
        return render_to_response('backend/filter_subscribers.html',{'sld_cancelled_subscriptions_dict':sld_cancelled_subscriptions_dict,
                                                              'ssd_cancelled_subscriptions_dict':ssd_cancelled_subscriptions_dict,
                                                              'sld_subscriptions_dict': sld_subscriptions_dict,
                                                              'ssd_subscriptions_dict': ssd_subscriptions_dict},context_instance = RequestContext(request))

@login_required
def message_popup(request,msg_id,db):
    private = False;
    if(db == 'sld'):
        private = True;
        msg = SLDMessage.objects.all().using('sld').get(id=msg_id)
        if msg.is_replied:
            reply = SLDMessage.objects.all().using('sld').get(parent_id=msg_id)
        else:
            reply = None
        date_send = msg.sent
    elif(db == 'sldp'):
        msg = SLDPublicmessage.objects.all().using('sld').get(id=msg_id)
        if msg.is_replied:
            reply = SLDPublicmessage.objects.all().using('sld').get(parent_id=msg_id)
        else:
            reply = None
        date_send = msg.date_sent
    elif(db == 'ssd'):
        private = True;
        msg = SSDMessage.objects.all().using('ssd').get(id=msg_id)
        if msg.is_replied:
            reply = SSDMessage.objects.all().using('sld').get(parent_id=msg_id)
        else:
            reply = None
        date_send = msg.sent
    elif(db == 'ssdp'):
        msg = SSDPublicmessage.objects.all().using('ssd').get(id=msg_id)
        if msg.is_replied:
            reply = SSDPublicmessage.objects.all().using('sld').get(parent_id=msg_id)
        else:
            reply = None
        date_send = msg.date_sent
    else:
        msg = Message.objects.get(id=msg_id)
        if msg.is_replied:
            reply = Message.objects.all().get(parent_id=msg_id)
        else:
            reply = None
        date_send = msg.date_sent

    allow_replied = not msg.is_replied and msg.received

    is_replied = msg.is_replied

    msg_header = ''

    if msg.received:
        if private:
            msg_header = 'Received from %s %s in %s'%(msg.user.first_name, msg.user.last_name, date_send.today().strftime('%d-%m-%y'))
        else:
            msg_header = 'Received from %s in %s'%(msg.user_name,date_send.today().strftime('%d-%m-%y'))
    else:
        if private:
            msg_header = 'Sent to %s %s in %s'%(msg.reply_first_name, msg.reply_last_name, date_send.today().strftime('%d-%m-%y'))
        else:
            msg_header = 'Sent to %s in %s'%(msg.user_name,date_send.today().strftime('%d-%m-%y'))

    if(db != 'admin'):
        subject =  msg.subject
    else:
        subject = ''

    body = msg.body

    return render_to_response('backend/view_message.html',{'allow_replied':allow_replied,
                                                           'is_replied':is_replied,
                                                           'msg_header':msg_header,
                                                           'subject':subject,
                                                           'msg_id':msg.id,
                                                           'body':body,
                                                           'db':db,
                                                           'reply':reply},context_instance = RequestContext(request))

@login_required
def reply_msg(request,msg_id,db):
    if request.method == 'POST':
        if(db == 'sld'):
            msg = SLDMessage.objects.all().using('sld').get(id=msg_id)
            msg_reply = SLDMessage()
            msg_reply.body = request.POST['body']
            msg_reply.subject = msg.subject
            msg_reply.parent_id = msg.id
            user = User.objects.all().using('ssd').get(id=1)
            msg_reply.user = user
            if user.first_name != '':
                msg_reply.reply_first_name = user.first_name
            else:
                msg_reply.reply_first_name = 'Smoking Liquid Admin'
            if user.last_name != '':
                msg_reply.reply_last_name = user.last_name
            else:
                msg_reply.reply_last_name = 'Admin'
            to_value = [msg.user.email]
            msg_reply.received = False
            msg_reply.sent = datetime.datetime.now()
            msg_reply.save(using='sld')
            msg.is_replied = True
            msg.save(using='sld')
        elif(db == 'sldp'):
            msg = SLDPublicmessage.objects.all().using('sld').get(id=msg_id)
            msg_reply = SLDPublicmessage()
            msg_reply.body = request.POST['body']
            msg_reply.subject = msg.subject
            msg_reply.parent_id = msg.id
            msg_reply.user_name = request.user.first_name + ' ' + request.user.last_name
            msg_reply.received = False
            msg_reply.date_sent = datetime.datetime.now()
            msg_reply.save(using='sld')
            msg.is_replied = True
            to_value = [msg.user_email]
            msg.save(using='sld')
        elif(db == 'ssd'):
            private = True;
            msg = SSDMessage.objects.all().using('ssd').get(id=msg_id)
            msg_reply = SSDMessage()
            msg_reply.body = request.POST['body']
            msg_reply.subject = msg.subject
            msg_reply.parent_id = msg.id
            user = User.objects.all().using('ssd').get(id=1)
            msg_reply.user = user
            if user.first_name != '':
                msg_reply.reply_first_name = user.first_name
            else:
                msg_reply.reply_first_name = 'Stop Smoking Admin'
            if user.last_name != '':
                msg_reply.reply_last_name = user.last_name
            else:
                msg_reply.reply_last_name = 'Admin'
            msg_reply.received = False
            to_value = [msg.user.email]
            msg_reply.sent = datetime.datetime.now()
            msg_reply.save(using='ssd')
            msg.is_replied = True
            msg.save(using='ssd')
        elif(db == 'ssdp'):
            msg = SSDPublicmessage.objects.all().using('ssd').get(id=msg_id)
            msg_reply = SSDPublicmessage()
            msg_reply.body = request.POST['body']
            msg_reply.subject = msg.subject
            msg_reply.parent_id = msg.id
            msg_reply.user_name = request.user.first_name + ' ' + request.user.last_name
            msg_reply.received = False
            msg_reply.date_sent = datetime.datetime.now()
            msg_reply.save(using='ssd')
            msg.is_replied = True
            to_value = [msg.user_email]
            msg.save(using='ssd')
        else:
            msg = Message.objects.get(id=msg_id)
            msg_reply = Message()
            msg_reply.body = request.POST['body']
            msg_reply.parent_id = msg.id
            msg_reply.user_name = request.user.first_name + ' ' + request.user.last_name
            msg_reply.received = False
            msg_reply.date_sent = datetime.datetime.now()
            to_value = [msg.user_email]
            msg_reply.save()
            msg.is_replied = True
            msg.save()

    subject_value = 'Message replied'
    message_value = request.POST['body']
    if settings.USE_SSL:
        try:
            smt = smtplib.SMTP_SSL(host=settings.EMAIL_HOST,port=settings.EMAIL_PORT)
            message = EmailMessage(subject_value,message_value,from_email=settings.EMAIL_HOST_USER,to=to_value)
            smt.sendmail(settings.EMAIL_HOST_USER,to_value,message.message().as_string())
            smt.close()
        except Exception:
            return HttpResponseRedirect('/ouradmin/messages/')
    else:
        try:
            send_mail(subject_value, message_value, settings.FROM_USER,to_value)
        except Exception:
            return HttpResponseRedirect('/ouradmin/messages/')
    return HttpResponseRedirect('/ouradmin/messages/')

@login_required
def get_message_by_name(request,filter_name):
    sldmsg = SLDMessage.objects.all().using('sld')
    sldmsg_dict = {}
    for msg in sldmsg:
        if msg.received:
            if msg.user.sldgeneraluser_set.all()[0].your_firstname.lower().find(filter_name.lower()) != -1 or msg.user.sldgeneraluser_set.all()[0].your_secondname.lower().find(filter_name.lower()) != -1:
                status = ''
                if msg.received and msg.is_replied:
                    status = 'received (replied to)'
                elif msg.received and not msg.is_replied:
                    status = 'received'
                else:
                    status = 'sent'

                sldmsg_dict[msg.id] = (msg.user.first_name,msg.user.last_name,status,str(msg.sent.date().today().strftime('%d-%m-%y')),'sld')
        else:
            if msg.reply_first_name.lower().find(filter_name.lower()) != -1 or msg.reply_last_name.lower().find(filter_name.lower()) != -1:
                status = ''
                if msg.received and msg.is_replied:
                    status = 'received (replied to)'
                elif msg.received and not msg.is_replied:
                    status = 'received'
                else:
                    status = 'sent'

                sldmsg_dict[msg.id] = (msg.user.first_name,msg.user.last_name,status,str(msg.sent.date().today().strftime('%d-%m-%y')),'sld')
    sldpublicmsg = SLDPublicmessage.objects.all().using('sld')
    sldpublicmsg_dict = {}
    for msg in sldpublicmsg:
        if msg.user_name.lower().find(filter_name.lower()) != -1:
            status = ''
            if msg.received and msg.is_replied:
                status = 'received (replied to)'
            elif msg.received and not msg.is_replied:
                status = 'received'
            else:
                status = 'sent'
            sldpublicmsg_dict[msg.id] = (msg.user_name.split(' ')[0],msg.user_name.split(' ')[1],status,str(msg.date_sent.date().today().strftime('%d-%m-%y')),'sldp')
    ssdmsg = SSDMessage.objects.all().using('ssd')
    ssdmsg_dict = {}
    for msg in ssdmsg:
        if msg.received:
            if msg.user.ssdgeneraluser_set.all()[0].your_firstname.lower().find(filter_name.lower()) != -1 or msg.user.ssdgeneraluser_set.all()[0].your_secondname.lower().find(filter_name.lower()) != -1:
                status = ''
                if msg.received and msg.is_replied:
                    status = 'received (replied to)'
                elif msg.received and not msg.is_replied:
                    status = 'received'
                else:
                    status = 'sent'
                ssdmsg_dict[msg.id] = (msg.user.first_name,msg.user.last_name,status,str(msg.sent.date().today().strftime('%d-%m-%y')),'ssd')
        else:
            if msg.reply_first_name.lower().find(filter_name.lower()) != -1 or msg.reply_last_name.lower().find(filter_name.lower()) != -1:
                status = ''
                if msg.received and msg.is_replied:
                    status = 'received (replied to)'
                elif msg.received and not msg.is_replied:
                    status = 'received'
                else:
                    status = 'sent'
                ssdmsg_dict[msg.id] = (msg.user.first_name,msg.user.last_name,status,str(msg.sent.date().today().strftime('%d-%m-%y')),'ssd')
    ssdpublicmsg = SSDPublicmessage.objects.all().using('ssd')
    ssdpublicmsg_dict = {}
    for msg in ssdpublicmsg:
        if msg.user_name.lower().find(filter_name.lower()) != -1:
            status = ''
            if msg.received and msg.is_replied:
                status = 'received (replied to)'
            elif msg.received and not msg.is_replied:
                status = 'received'
            else:
                status = 'sent'

            ssdpublicmsg_dict[msg.id] = (msg.user_name.split(' ')[0],msg.user_name.split(' ')[1],status,str(msg.date_sent.date().today().strftime('%d-%m-%y')),'ssdp')
    adminmsg = Message.objects.all()
    adminmsg_dict = {}
    for msg in adminmsg:
        if msg.user_name.lower().find(filter_name.lower()) != -1:
            status = ''
            if msg.received and msg.is_replied:
                status = 'received (replied to)'
            elif msg.received and not msg.is_replied:
                status = 'received'
            else:
                status = 'sent'
            adminmsg_dict[msg.id] = (msg.user_name.split(' ')[0],msg.user_name.split(' ')[1],status,str(msg.date_sent.date().today().strftime('%d-%m-%y')),'admin')
    return render_to_response('backend/filter_messages.html',{'sldmsg_dict':sldmsg_dict,'sldpublicmsg_dict':sldpublicmsg_dict,'ssdmsg_dict':ssdmsg_dict,'ssdpublicmsg_dict':ssdpublicmsg_dict,'adminmsg_dict':adminmsg_dict},context_instance = RequestContext(request))

def get_message_by_postcode(request,filter_name):
    sldmsg = SLDMessage.objects.all().using('sld')
    sldmsg_dict = {}
    for msg in sldmsg:
        if msg.received:
            if msg.user.sldgeneraluser_set.all()[0].your_postcode.lower().find(filter_name.lower()) != -1:
                status = ''
                if msg.received and msg.is_replied:
                    status = 'received (replied to)'
                elif msg.received and not msg.is_replied:
                    status = 'received'
                else:
                    status = 'sent'

                sldmsg_dict[msg.id] = (msg.user.first_name,msg.user.last_name,status,str(msg.sent.date().today().strftime('%d-%m-%y')),'sld')

    ssdmsg = SSDMessage.objects.all().using('ssd')
    ssdmsg_dict = {}
    for msg in ssdmsg:
        if msg.received:
            if msg.user.ssdgeneraluser_set.all()[0].your_postcode.lower().find(filter_name.lower()) != -1:
                status = ''
                if msg.received and msg.is_replied:
                    status = 'received (replied to)'
                elif msg.received and not msg.is_replied:
                    status = 'received'
                else:
                    status = 'sent'
                ssdmsg_dict[msg.id] = (msg.user.first_name,msg.user.last_name,status,str(msg.sent.date().today().strftime('%d-%m-%y')),'ssd')
    return render_to_response('backend/filter_messages.html',{'sldmsg_dict':sldmsg_dict,'sldpublicmsg_dict':{},'ssdmsg_dict':ssdmsg_dict,'ssdpublicmsg_dict':{},'adminmsg_dict':{}},context_instance = RequestContext(request))

@login_required
def get_message_by_status(request,pstatus):
    pstatus = int(pstatus)
    sldmsg = SLDMessage.objects.all().using('sld')
    sldmsg_dict = {}
    for msg in sldmsg:
        status = ''
        cmpstatus = ''
        if msg.received and msg.is_replied:
            status = 'received (replied to)'
            cmpstatus = 1
        elif msg.received and not msg.is_replied:
            status = 'received'
            cmpstatus = 1
        else:
            status = 'sent'
            cmpstatus = 2
        if cmpstatus == pstatus:
            sldmsg_dict[msg.id] = (msg.user.first_name,msg.user.last_name,status,str(msg.sent.date().today().strftime('%d-%m-%y')),'sld')
    sldpublicmsg_dict = {}
    sldpublicmsg = SLDPublicmessage.objects.all().using('sld')
    for msg in sldpublicmsg:
        status = ''
        cmpstatus = ''
        if msg.received and msg.is_replied:
            status = 'received (replied to)'
            cmpstatus = 1
        elif msg.received and not msg.is_replied:
            status = 'received'
            cmpstatus = 1
        else:
            status = 'sent'
            cmpstatus = 2
        if cmpstatus == pstatus:
            sldpublicmsg_dict[msg.id] = (msg.user_name.split(' ')[0],msg.user_name.split(' ')[1],status,str(msg.date_sent.date().today().strftime('%d-%m-%y')),'sldp')
    ssdmsg = SSDMessage.objects.all().using('ssd')
    ssdmsg_dict = {}
    for msg in ssdmsg:
        status = ''
        cmpstatus = ''
        if msg.received and msg.is_replied:
            status = 'received (replied to)'
            cmpstatus = 1
        elif msg.received and not msg.is_replied:
            status = 'received'
            cmpstatus = 1
        else:
            status = 'sent'
            cmpstatus = 2
        if cmpstatus == pstatus:
            ssdmsg_dict[msg.id] = (msg.user.first_name,msg.user.last_name,status,str(msg.sent.date().today().strftime('%d-%m-%y')),'ssd')
    ssdpublicmsg_dict = {}
    ssdpublicmsg = SSDPublicmessage.objects.all().using('ssd')
    for msg in ssdpublicmsg:
        status = ''
        cmpstatus = ''
        if msg.received and msg.is_replied:
            status = 'received (replied to)'
            cmpstatus = 1
        elif msg.received and not msg.is_replied:
            status = 'received'
            cmpstatus = 1
        else:
            status = 'sent'
            cmpstatus = 2
        if cmpstatus == pstatus:
            ssdpublicmsg_dict[msg.id] = (msg.user_name.split(' ')[0],msg.user_name.split(' ')[1],status,str(msg.date_sent.date().today().strftime('%d-%m-%y')),'ssdp')
    adminmsg_dict = {}
    adminmsg = Message.objects.all()
    for msg in adminmsg:
        status = ''
        cmpstatus = ''
        if msg.received and msg.is_replied:
            status = 'received (replied to)'
            cmpstatus = 1
        elif msg.received and not msg.is_replied:
            status = 'received'
            cmpstatus = 1
        else:
            status = 'sent'
            cmpstatus = 2
        if cmpstatus == pstatus:
            adminmsg_dict[msg.id] = (msg.user_name.split(' ')[0],msg.user_name.split(' ')[1],status,str(msg.date_sent.date().today().strftime('%d-%m-%y')),'admin')
    return render_to_response('backend/filter_messages.html',{'sldmsg_dict':sldmsg_dict,'sldpublicmsg_dict':sldpublicmsg_dict,'ssdmsg_dict':ssdmsg_dict,'ssdpublicmsg_dict':ssdpublicmsg_dict,'adminmsg_dict':adminmsg_dict},context_instance = RequestContext(request))

@login_required
def get_messages(request):
    sldmsg = SLDMessage.objects.all().using('sld')
    sldmsg_dict = {}
    for msg in sldmsg:
        status = ''
        if msg.received and msg.is_replied:
            status = 'received (replied to)'
        elif msg.received and not msg.is_replied:
            status = 'received'
        else:
            status = 'sent'

        sldmsg_dict[msg.id] = (msg.user.first_name,msg.user.last_name,status,str(msg.sent.date().today().strftime('%d-%m-%y')),'sld')
    sldpublicmsg = SLDPublicmessage.objects.all().using('sld')
    sldpublicmsg_dict = {}
    for msg in sldpublicmsg:
        status = ''
        if msg.received and msg.is_replied:
            status = 'received (replied to)'
        elif msg.received and not msg.is_replied:
            status = 'received'
        else:
            status = 'sent'
        sldpublicmsg_dict[msg.id] = (msg.user_name.split(' ')[0],msg.user_name.split(' ')[1],status,str(msg.date_sent.date().today().strftime('%d-%m-%y')),'sldp')
    ssdmsg = SSDMessage.objects.all().using('ssd')
    ssdmsg_dict = {}
    for msg in ssdmsg:
        status = ''
        if msg.received and msg.is_replied:
            status = 'received (replied to)'
        elif msg.received and not msg.is_replied:
            status = 'received'
        else:
            status = 'sent'
        ssdmsg_dict[msg.id] = (msg.user.first_name,msg.user.last_name,status,str(msg.sent.date().today().strftime('%d-%m-%y')),'ssd')
    ssdpublicmsg = SSDPublicmessage.objects.all().using('ssd')
    ssdpublicmsg_dict = {}
    for msg in ssdpublicmsg:
        status = ''
        if msg.received and msg.is_replied:
            status = 'received (replied to)'
        elif msg.received and not msg.is_replied:
            status = 'received'
        else:
            status = 'sent'

        ssdpublicmsg_dict[msg.id] = (msg.user_name.split(' ')[0],msg.user_name.split(' ')[1],status,str(msg.date_sent.date().today().strftime('%d-%m-%y')),'ssdp')
    adminmsg = Message.objects.all()
    adminmsg_dict = {}
    for msg in adminmsg:
        status = ''
        if msg.received and msg.is_replied:
            status = 'received (replied to)'
        elif msg.received and not msg.is_replied:
            status = 'received'
        else:
            status = 'sent'
        adminmsg_dict[msg.id] = (msg.user_name.split(' ')[0],msg.user_name.split(' ')[1],status,str(msg.date_sent.date().today().strftime('%d-%m-%y')),'admin')
    return render_to_response('backend/filter_messages.html',{'sldmsg_dict':sldmsg_dict,'sldpublicmsg_dict':sldpublicmsg_dict,'ssdmsg_dict':ssdmsg_dict,'ssdpublicmsg_dict':ssdpublicmsg_dict,'adminmsg_dict':adminmsg_dict},context_instance = RequestContext(request))