from django.conf.urls import patterns

urlpatterns = patterns('',
   (r'^$', 'backend.views.home'),
   (r'^home/$', 'backend.views.home'),
   (r'^orders/$', 'backend.views.orders'),
   (r'^reports/$', 'backend.views.reports'),
   (r'^stock/$', 'backend.views.stock'),
   (r'^subscribers/$', 'backend.views.subscribers'),
   (r'^messages/$', 'backend.views.messages'),
   (r'^users/$', 'backend.views.users'),
   (r'^subs_popup/(?P<subs_id>\d+)/(?P<db>.*)$', 'backend.views.subs_popup'),
   (r'^autorize_cancellation/(?P<subs_id>\d+)/(?P<db>.*)$', 'backend.views.autorize_cancellation'),
   (r'^print_order/(?P<order_id>\d+)/(?P<db>.*)$', 'backend.views.print_order'),
   (r'^view_order/(?P<order_id>\d+)/(?P<db>.*)$', 'backend.views.view_order'),
   (r'^print_order_html/(?P<order_id>\d+)/(?P<db>.*)$', 'backend.views.print_order_as_html'),
   (r'^print_batch/(?P<batch_id>\d+)/(?P<db>.*)$', 'backend.views.print_batch'),
   (r'^print_batch_html/(?P<batch_id>\d+)/(?P<db>.*)$', 'backend.views.print_batch_as_html'),
   (r'^print_all_orders/$', 'backend.views.print_all_orders'),
   (r'^print_all_orders_html/$', 'backend.views.print_all_orders_as_html'),
   (r'^get_orders_by_batch/(?P<batch_id>.*)$', 'backend.views.get_orders_by_batch'),
   (r'^get_orders_by_number/(?P<order_number>\d+)$', 'backend.views.get_orders_by_number'),
   (r'^get_orders/$', 'backend.views.get_orders'),
   (r'^get_pending_orders/$', 'backend.views.get_pending_orders'),
   (r'^get_subscribers_by_name/(?P<filter_name>.*)$', 'backend.views.get_subscriber_by_name'),
   (r'^get_subscribers_by_postcode/(?P<filter_name>.*)$', 'backend.views.get_subscriber_by_postcode'),
   (r'^get_subscribers_by_status/(?P<status>\d+)$', 'backend.views.get_subscribers_by_status'),
   (r'^get_subscribers/$', 'backend.views.get_subscribers'),
   (r'^get_message_by_name/(?P<filter_name>.*)$', 'backend.views.get_message_by_name'),
   (r'^get_message_by_postcode/(?P<filter_name>.*)$', 'backend.views.get_message_by_postcode'),
   (r'^get_message_by_status/(?P<pstatus>\d+)$', 'backend.views.get_message_by_status'),
   (r'^get_messages/$', 'backend.views.get_messages'),
   (r'^message_popup/(?P<msg_id>\d+)/(?P<db>.*)$', 'backend.views.message_popup'),
   (r'^reply_msg/(?P<msg_id>\d+)/(?P<db>.*)$', 'backend.views.reply_msg'),
)