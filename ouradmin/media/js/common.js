function email_validation(email_value){
    var filter = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
    if(filter.test(email_value.val()))
    {
        return true;
    }
    else
    {
        email_value.addClass('input_error');
        return false;
    }
}

