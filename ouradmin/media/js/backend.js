$(document).ready(function(){
        $('#subscribers_popup_tag').jqm();
        $('#message_view_tag').jqm();
        $('#print_processing').jqm();
        $('#searching_process').jqm();
        $('#subscribers_popup_tag').css('top','8%');
        $('#subscribers_popup_tag').css('position','absolute');
        $(document).focusin(function(){
            $('#print_processing').jqmHide();
        });

        $("#option_selecter option:first").attr('selected','selected');

        $("#option_selecter").change(function(){
           if($("#option_selecter option:selected").val() == '1' || $("#option_selecter option:selected").val() == '2'){
               $("#filter_text").val("");
               $("#send_form").show();
           }
           else if($("#option_selecter option:selected").val() == '3'){
               $("#send_form").hide();
               $.get("/ouradmin/get_pending_orders/", function (data) {
                   $('#order_wrapper_content').html(data);
               });
           }
           else{
               $("#send_form").hide();
               $('#searching_process').jqmShow();
               $.get("/ouradmin/get_orders/", function (data) {
                   $.ajaxSetup({async:false});
                   $('#order_wrapper_content').html(data);
                   $('#searching_process').jqmHide();
               });
           }
        });

        $("#option_selecter_subs option:first").attr('selected','selected');

        $("#option_selecter_subs").change(function(){
           if($("#option_selecter_subs option:selected").val() == '1' || $("#option_selecter_subs option:selected").val() == '2'){
               $("#filter_text_subs").val("");
               $("#send_form").show();
           }
           else{
               $("#send_form").hide();
               $('#searching_process').jqmShow();
               $.get('/ouradmin/get_subscribers/', function (data) {
                   $.ajaxSetup({async:false});
                   $("#subs_content").html(data);
                   $('#searching_process').jqmHide();
               });
           }
        });

         $("#option_selecter_msg option:first").attr('selected','selected');

        $("#option_selecter_msg").change(function(){
           if($("#option_selecter_msg option:selected").val() == '1' || $("#option_selecter_msg option:selected").val() == '2'){
               $("#filter_text_msg").val("");
               $("#send_form").show();
           }
           else{
               $("#send_form").hide();
               $('#searching_process').jqmShow();
               $.get('/ouradmin/get_messages/', function (data) {
                   $.ajaxSetup({async:false});
                   $("#msg_content").html(data);
                   $('#searching_process').jqmHide();
               });
           }
        });

        $("#filter_text").keyup(function(event){
            if(event.keyCode == 13){
                if($("#option_selecter option:selected").val() == '1'){
                   if ($("#filter_text").val() != '') {
                       $('#searching_process').jqmShow();
                       $.ajaxSetup({async:false});
                       $.get("/ouradmin/get_orders_by_batch/" + $("#filter_text").val(), function (data) {
                           $('#order_wrapper_content').html(data);
                           $('#searching_process').jqmHide();
                       });
                   }
                   else{
                       $('#searching_process').jqmShow();
                       $.ajaxSetup({async:false});
                       $.get("/ouradmin/get_orders/", function (data) {
                           $('#order_wrapper_content').html(data);
                           $('#searching_process').jqmHide();
                       });
                   }

               }
               else{
                   if ($("#filter_text").val() != '' && !isNaN($("#filter_text").val())) {
                       $('#searching_process').jqmShow();
                       $.ajaxSetup({async:false});
                       $.get("/ouradmin/get_orders_by_number/" + $("#filter_text").val(), function (data) {
                           $('#order_wrapper_content').html(data);
                           $('#searching_process').jqmHide();
                       });
                   }
                   else if($("#filter_text").val() == ''){
                       $('#searching_process').jqmShow();
                       $.ajaxSetup({async:false});
                       $.get("/ouradmin/get_orders/", function (data) {
                            $('#order_wrapper_content').html(data);
                            $('#searching_process').jqmHide();
                       });
                   }
               }
            }
        });

        $("#filter_text_subs").keyup(function(event){
            if(event.keyCode == 13){
               if($("#option_selecter_subs option:selected").val() == '1'){
                   if($("#filter_text_subs").val() != ''){
                       $('#searching_process').jqmShow();
                       $.ajaxSetup({async:false});
                       $.get('/ouradmin/get_subscribers_by_name/' + $("#filter_text_subs").val(), function (data) {
                           $("#subs_content").html(data);
                           $('#searching_process').jqmHide();
                       });
                   }
                   else{
                       $('#searching_process').jqmShow();
                       $.ajaxSetup({async:false});
                       $.get('/ouradmin/get_subscribers/', function (data) {
                           $("#subs_content").html(data);
                           $('#searching_process').jqmHide();
                       });
                   }
               }
               else{
                   if($("#filter_text_subs").val() != ''){
                       $('#searching_process').jqmShow();
                       $.ajaxSetup({async:false});
                       $.get('/ouradmin/get_subscribers_by_postcode/' + $("#filter_text_subs").val(), function (data) {
                           $("#subs_content").html(data);
                           $('#searching_process').jqmHide();
                       });
                   }
                   else{
                       $('#searching_process').jqmShow();
                       $.ajaxSetup({async:false});
                       $.get('/ouradmin/get_subscribers/', function (data) {
                           $("#subs_content").html(data);
                           $('#searching_process').jqmHide();
                       });
                   }
               }
            }
        });


        $("#filter_text_msg").keyup(function(event){
            if(event.keyCode == 13){
               if($("#option_selecter_msg option:selected").val() == '1'){
                   if($("#filter_text_msg").val() != ''){
                       $('#searching_process').jqmShow();
                       $.ajaxSetup({async:false});
                       $.get('/ouradmin/get_message_by_name/' + $("#filter_text_msg").val(), function (data) {
                           $("#msg_content").html(data);
                           $('#searching_process').jqmHide();
                       });
                   }
                   else{
                       $('#searching_process').jqmShow();
                       $.ajaxSetup({async:false});
                       $.get('/ouradmin/get_messages/', function (data) {
                           $("#msg_content").html(data);
                           $('#searching_process').jqmHide();
                       });
                   }
               }
               else{
                   if($("#filter_text_msg").val() != ''){
                       $('#searching_process').jqmShow();
                       $.ajaxSetup({async:false});
                       $.get('/ouradmin/get_message_by_postcode/' + $("#filter_text_msg").val(), function (data) {
                           $("#msg_content").html(data);
                           $('#searching_process').jqmHide();
                       });
                   }
                   else{
                       $('#searching_process').jqmShow();
                       $.ajaxSetup({async:false});
                       $.get('/ouradmin/get_messages/', function (data) {
                           $("#msg_content").html(data);
                           $('#searching_process').jqmHide();
                       });
                   }
               }
            }
        });

        $("#status_subs option:first").attr("selected","selected");

        $("#status_subs").change(function(){
            if($("#status_subs option:selected").val() != '0'){
               $('#searching_process').jqmShow();
               $.ajaxSetup({async:false});
               $.get('/ouradmin/get_subscribers_by_status/' + $("#status_subs option:selected").val(), function (data) {
                   $("#subs_content").html(data);
                   $('#searching_process').jqmHide();
               });
            }
            else{
                $('#searching_process').jqmShow();
                $.ajaxSetup({async:false});
                $.get('/ouradmin/get_subscribers/', function (data) {
                     $("#subs_content").html(data);
                     $('#searching_process').jqmHide();
                   });
            }
        });

        $("#status_msg option:first").attr("selected","selected");

        $("#status_msg").change(function(){
            if($("#status_msg option:selected").val() != '0'){
               $('#searching_process').jqmShow();
               $.ajaxSetup({async:false});
               $.get('/ouradmin/get_message_by_status/' + $("#status_msg option:selected").val(), function (data) {
                   $("#msg_content").html(data);
                   $('#searching_process').jqmHide();
               });
            }
            else{
                $('#searching_process').jqmShow();
                $.ajaxSetup({async:false});
                $.get('/ouradmin/get_messages/', function (data) {
                     $("#msg_content").html(data);
                     $('#searching_process').jqmHide();
                   });
            }
        });

});

function viewlist(dclassname,linkid){
    if( $("#" + linkid).html() == 'View List'){
         $("." + dclassname).css("display","block");
         $("#" + linkid).html("View Group");
    }
    else{
         $("." + dclassname).css("display","none");
         $("#" + linkid).html("View List");
    }
}

function view_subs(db,subs_id){
    $.get('/ouradmin/subs_popup/' + subs_id + '/' + db,function(data){
        $('#subscribers_popup_tag').html(data);
        $('#subscribers_popup_tag').jqmShow();
    })
}

function autorize_cancellation(db,subs_id){
   $.get('/ouradmin/autorize_cancellation/' + subs_id + '/' + db,function(data){
       $('#subscribers_popup_tag').jqmHide();
       $('#subscribers_popup_tag').html("");
       $('#main_div').html(data);
       $('#subscribers_popup_tag').jqm();
       $('#subscribers_popup_tag').css('top','8%');
       $('#subscribers_popup_tag').css('position','absolute');
    })
}

function print_order(order_id,db){
    $('#print_processing').jqmShow();
    $(location).attr("href","/ouradmin/print_order/" + order_id +"/" + db);
}

function print_order_as_html(urlpart,order_id,db){
    $('#print_processing').jqmShow();
    window.open(urlpart + "/ouradmin/print_order_html/" + order_id +"/" + db);
}

function print_batch(batch_id,db){
    $('#print_processing').jqmShow();
    $(location).attr("href","/ouradmin/print_batch/" + batch_id +"/" + db);
}


function print_batch_as_html(urlpart,batch_id,db){
    $('#print_processing').jqmShow();
    window.open(urlpart + "/ouradmin/print_batch_html/" + batch_id +"/" + db);
}

function print_all_orders(){
    $('#print_processing').jqmShow();
    $(location).attr("href","/ouradmin/print_all_orders/");
}

function print_all_orders_as_html(){
    $('#print_processing').jqmShow();
     window.open(urlpart + "/ouradmin/print_all_orders_html/");
}

function view_order(order_id,db){
    $('#print_processing').jqmShow();
    var url = "";
    $.ajaxSetup({async:false});
    $.get("/ouradmin/view_order/" + order_id +"/" + db,function(data){
        url = data.pdf_path;
        $('#print_processing').jqmHide();
    });
    window.open(url);
}

function view_message(msg_id,db){
    $.get('/ouradmin/message_popup/' + msg_id + '/' + db,function(data){
        $('#message_view_tag').html(data);
        $('#message_view_tag').jqmShow();
    })
}

function show_replied_form(){
    if($("#display_form").html() == 'Reply'){
        $("#display_form").html('Not reply');
        $("#replied_form").show();
    }
    else{
        $("#display_form").html('Reply');
        $("#replied_form").hide();
    }

}